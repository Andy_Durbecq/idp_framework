
        </div>        
        
        <footer<?php if($this->application->action == "formConnexionAction") echo " id='footer-connexion'"; ?>>
            
            <div>
                &copy; <a href="http://www.id-parallele.com" target="_blank">ID Parallèle</a> <?php echo date("Y"); ?> - Une question ? <a href="mailto:contact@id-parallele.com">Contactez-nous</a>
            </div>
            
            <div id="footer-social">
                <a href="https://www.facebook.com/Agence.IDParallele?ref=hl" target="_blank"><i class="fa fa-facebook fa-fw"></i></a>
                <a href="https://twitter.com/idparallele" target="_blank"><i class="fa fa-twitter fa-fw"></i></a>
                <a href="https://www.youtube.com/user/idparallele" target="_blank"><i class="fa fa-youtube fa-fw"></i></a>
            </div>
            
        </footer>
    </body>
</html>