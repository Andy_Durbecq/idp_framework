<!doctype html>
<html lang="fr">

    <head>
        <meta charset="utf-8" />
        <meta name="author" content="ID PARALLELE" />
        
        <meta name="robots" content="noindex,follow" />
        
        <title>Manager</title>
        <meta name="description" content="" />

        <link rel="shortcut icon" href="<?php echo $this->application->config->url; ?>/images/favicon.ico" />
        <link rel="stylesheet" href="<?php echo $this->application->config->url; ?>/css/font-awesome.min.css" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo $this->application->config->url; ?>/css/manager.css" />
        <?php
        foreach ($this->css as $style) {
            echo "<link rel=\"stylesheet\" href=\"" . $this->application->config->url . "/css/" . $style . "\" />";
        }
        ?>
        
        <!--[if lte IE 8]>
            <script type="text/javascript" src="<?php echo $this->application->config->url; ?>/javascript/html5.js"></script>
        <![endif]-->

        <script type="text/javascript" src="<?php echo $this->application->config->url; ?>/javascript/jquery.js"></script>
        <script type="text/javascript" src="<?php echo $this->application->config->url; ?>/javascript/modal-min.js"></script>
        <script type="text/javascript" src="<?php echo $this->application->config->url; ?>/javascript/manager.js"></script>
        <?php
        foreach ($this->javascript as $javascript) {
            echo "<script type=\"text/javascript\" src=\"" . $this->application->config->url . "/javascript/" . $javascript . "\"></script>";
        }
        ?>
    </head>

    <body>

        <div id="message-info"><span class="erreur"></span></div>

        <?php if ($this->manager) { ?>
            <div id="navigation">

                <div id="logo">
                    <a href="<?php echo $this->application->getUrl(); ?>/manager"><img src="<?php echo $this->application->config->url; ?>/images/manager/id-parallele.png" alt="ID Parallèle" /></a>
                    <span>Manager</span>
                </div>         
                
                <div id="navigation-langues">
                    <label>Langue à administrer : </label>
                    <select name="administration_langue" onchange="window.location.href=this.value;">
                        <?php
                        foreach ($this->application->config->languages as $language) {
                            echo "<option value=\"" . $this->application->config->url . "/" . $language . "/manager\"".(($this->application->language == $language) ? " selected=\"selected\"" : "").">" . $language . "</option>";
                        }
                        ?>
                    </select>
                </div>

                <div id="menu">
                    <ul>
                        <li><a href="<?php echo $this->application->getUrl(); ?>/manager"<?php if (!empty($this->datas['menu_actif']) && $this->datas['menu_actif'] == 1) echo " class='actif'"; ?>><i class="fa fa-tachometer fa-fw"></i>Tableau de bord</a></li>
                        <li><a href="<?php echo $this->application->getUrl(); ?>/manager/clients"<?php if (!empty($this->datas['menu_actif']) && $this->datas['menu_actif'] == 2) echo " class='actif'"; ?>><i class="fa fa-users fa-fw"></i>Clients</a></li>
                        <li><a href="<?php echo $this->application->getUrl(); ?>/manager/galerie-photo"<?php if (!empty($this->datas['menu_actif']) && $this->datas['menu_actif'] == 6) echo " class='actif'"; ?>><i class="fa fa-picture-o fa-fw"></i>Galerie Photo</a></li>
                        <li><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-actualite"<?php if (!empty($this->datas['menu_actif']) && $this->datas['menu_actif'] == 3 && !empty($this->datas['type']) && $this->datas['type'] == "actualite") echo " class='actif'"; ?>><i class="fa fa-newspaper-o fa-f<?php if (!empty($this->datas['menu_actif']) && $this->datas['menu_actif'] == 3) echo " class='actif'"; ?>w"></i>Actualités</a></li>                                                
                        <li><a href="<?php echo $this->application->getUrl(); ?>/manager/managers"<?php if (!empty($this->datas['menu_actif']) && $this->datas['menu_actif'] == 4) echo " class='actif'"; ?>><i class="fa fa-user-secret fa-fw"></i>Manager</a></li>                        
                        <li><a href="<?php echo $this->application->getUrl(); ?>/manager/logs-manager"<?php if (!empty($this->datas['menu_actif']) && $this->datas['menu_actif'] == 5) echo " class='actif'"; ?>><i class="fa fa-exclamation fa-fw"></i>Logs Manager</a></li>
                        <li><a href="<?php echo $this->application->getUrl(); ?>/manager/vider-le-cache"><i class="fa fa-trash-o fa-fw"></i>Vider le cache</a></li>
                        <li><a href="<?php echo $this->application->getUrl(); ?>/manager/deconnexion"><i class="fa fa-sign-out fa-fw"></i>Déconnexion</a></li>
                    </ul>
                </div>            

            </div>
        <?php } ?>

        <div id="wrap">            