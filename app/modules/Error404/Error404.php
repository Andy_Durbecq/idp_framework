<?php

namespace app\modules\Error404;

class Error404 extends \core\Module
{
    
    public function __construct(\core\Application $application, $datas = array())
    {
        parent::__construct($application, $datas);
        
        // We add the "template" folder of the "Error404" module to the list of directories containing templates twig
        $this->application->twig_loader->prependPath(__DIR__ . "/templates");
    }

    public function indexAction()
    {
        header("HTTP/1.0 404 Not Found");
        header("Status: 404 Not Found");
        
        echo $this->application->twig->render('404.twig');
    }
}
