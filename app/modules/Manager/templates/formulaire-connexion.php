<div id="page-connexion">

    <h1>Manager</h1>

    <form action="<?php echo $this->application->getUrl(); ?>/manager/connexion" method="post" autocomplete="off">
        <h2><img src="<?php echo $this->application->config->url; ?>/images/manager/id-parallele.png" alt="" /></h2>
        <p>
            <label for="email">E-mail</label>
            <span>
                <i class="fa fa-envelope fa-fw"></i>
                <input type="text" name="email" tabindex="1" autocomplete="off" id="email" value="<?php if (!empty($this->datas['erreur']) && !empty($_POST['email'])) echo htmlspecialchars($_POST['email']); ?>" />
            </span>
        </p>
        <p>
            <label for="pass">Mot de passe</label>
            <span>
                <i class="fa fa-key fa-fw"></i>
                <input type="password" name="pass" tabindex="2" autocomplete="off" id="pass"/>
            </span>
        </p>
        <p id="p-submit">
            <input type="submit" value="Se connecter" tabindex="3" />
            <input type="hidden" name="token" value="<?php echo \core\Securite::getToken(); ?>" />
        </p>
    </form>
    
</div>