<div id="wrap-container">
    <div id="page-clients">

        <h1>Actualités</h1> 

        <div id="barre-outils">
            <div id="barre-outils-actions">
                <a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/ajouter-un-post/retour-page-<?php echo $this->page; ?>/tri-<?php echo $this->sens_tri; ?>-<?php echo $this->champ_tri; ?>"><i class="fa fa-fw fa-plus" style="color: #ffffff;"></i>Ajouter</a>
            </div>
        </div>

        <section>
            <div class="section-container">
                <h2>Liste &nbsp;(<?php echo $this->total; ?>)</h2>

                <table>
                    <tbody>
                        <tr>
                            <th>ID <span class="tri"><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-desc-id"<?php if($this->sens_tri == "desc" && $this->champ_tri == "id") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-up"></i></a><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-asc-id"<?php if($this->sens_tri == "asc" && $this->champ_tri == "id") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-down"></i></a></span></th>
                            <th>Date <span class="tri"><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-desc-date"<?php if($this->sens_tri == "desc" && $this->champ_tri == "date") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-up"></i></a><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-asc-date"<?php if($this->sens_tri == "asc" && $this->champ_tri == "date") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-down"></i></a></span></th>   
                            <th>Titre <span class="tri"><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-desc-titre"<?php if($this->sens_tri == "desc" && $this->champ_tri == "titre") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-up"></i></a><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-asc-titre"<?php if($this->sens_tri == "asc" && $this->champ_tri == "titre") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-down"></i></a></span></th>
                            <th>Ordre <span class="tri"><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-desc-ordre"<?php if($this->sens_tri == "desc" && $this->champ_tri == "ordre") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-up"></i></a><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-asc-ordre"<?php if($this->sens_tri == "asc" && $this->champ_tri == "ordre") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-down"></i></a></span></th>                          
                            <th>Actif <span class="tri"><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-desc-actif"<?php if($this->sens_tri == "desc" && $this->champ_tri == "actif") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-up"></i></a><a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/page-<?php echo $this->page; ?>/tri-asc-actif"<?php if($this->sens_tri == "asc" && $this->champ_tri == "actif") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-down"></i></a></span></th>                          
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <?php
                        foreach ($this->posts as $post) {
                            echo "<tr>";
                            echo "<td class='center'>" . $post->id . "</td>";
                            echo "<td>" . date('d/m/Y', $post->date) . "</td>";
                            echo "<td>" . $post->titre . "</td>";
                            echo "<td class='center'>" . $post->ordre . "</td>";
                            echo "<td class='center'>" . (($post->actif) ? "<i class='fa fa-fw fa-check' style='color: #00c921;'></i>" : "<i class='fa fa-fw fa-close' style='color: #ff0000;'></i>") . "</td>";
                            echo "<td class='center'><a href='" . $this->application->getUrl() . "/manager/posts-" . $this->datas['type'] . "/dupliquer-un-post/" . $post->id . "/retour-page-" . $this->page . "/tri-" . $this->sens_tri . "-" . $this->champ_tri . "' title=\"Dupliquer\"><i class='fa fa-fw fa-clone'></i></a></td>";
                            echo "<td class='center'><a href='" . $this->application->getUrl() . "/manager/posts-" . $this->datas['type'] . "/modifier-un-post/" . $post->id . "/retour-page-" . $this->page . "/tri-" . $this->sens_tri . "-" . $this->champ_tri . "' title=\"Modifier\"><i class='fa fa-fw fa-pencil-square-o'></i></a></td>";
                            echo "<td class='center'><a href='" . $this->application->getUrl() . "/manager/posts-" . $this->datas['type'] . "/supprimer-un-post/" . $post->id . "/retour-page-" . $this->page . "/tri-" . $this->sens_tri . "-" . $this->champ_tri . "' title=\"Supprimer\" onclick=\"return confirm('Voulez-vous vraiment supprimer ?');\"><i class='fa fa-fw fa-trash-o'></i></a></td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>                       
                </table>
            </div>
        </section>

        <div id="pagination">
            <?php
            $debut_pagination = ($this->page - 5 >= 1) ? $this->page - 5 : 1;
            $fin_pagination = ($this->page + 5 <= ceil($this->total / $this->limite_par_page)) ? $this->page + 5 : ceil($this->total / $this->limite_par_page);
            for ($i = $debut_pagination; $i <= $fin_pagination; $i++) {
                if ($i == $this->page) {
                    echo "<a href='" . $this->application->getUrl() . "/manager/posts-" . $this->datas['type'] . "/page-" . $i . "/tri-" . $this->sens_tri . "-" . $this->champ_tri . "' class='actif'>" . $i . "</a>";
                } else {
                    echo "<a href='" . $this->application->getUrl() . "/manager/posts-" . $this->datas['type'] . "/page-" . $i . "/tri-" . $this->sens_tri . "-" . $this->champ_tri . "'>" . $i . "</a>";
                }
            }
            ?>
        </div>

    </div>
</div>