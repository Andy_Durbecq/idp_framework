<div id="wrap-container">
    <div id="page-clients">

        <h1>Modifier un client</h1> 

        <div id="barre-outils">
            <div id="barre-outils-form">
                <a href="<?php echo $this->application->getUrl(); ?>/manager/clients/<?php echo $this->datas['retour_url']; ?>"><i class="fa fa-fw fa-backward"></i>Retour à la liste des clients</a>
            </div>
        </div>

        <form action="<?php echo $this->application->getUrl(); ?>/manager/clients/modifier-un-client/<?php echo $this->client->id; ?>/retour-<?php echo $this->datas['retour_url']; ?>" method="post" autocomplete="off">
            <section>
                <div class="section-container">
                    <h2>Informations sur le client</h2>

                    <p>
                        <label for="nom">Nom *</label>
                        <span>
                            <input type="text" name="nom" tabindex="1" autocomplete="off" id="nom" value="<?php if (!empty($_POST['nom'])) echo htmlspecialchars($_POST['nom']); else echo htmlspecialchars($this->client->nom); ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="prenom">Prénom *</label>
                        <span>
                            <input type="text" name="prenom" tabindex="2" autocomplete="off" id="prenom" value="<?php if (!empty($_POST['prenom'])) echo htmlspecialchars($_POST['prenom']); else echo htmlspecialchars($this->client->prenom); ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="email">E-mail *</label>
                        <span>
                            <input type="text" name="email" tabindex="3" autocomplete="off" id="email" value="<?php if (!empty($_POST['email'])) echo htmlspecialchars($_POST['email']); else echo htmlspecialchars($this->client->email); ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="pass">Mot de passe (A renseigner uniquement si vous souhiater le modifier)</label>
                        <span>
                            <input type="password" name="pass" tabindex="4" autocomplete="off" id="pass"/>
                        </span>
                    </p>     
                    <p class="p-checkbox-radio">
                        <input type="checkbox" value="1" tabindex="5" name="actif" id="actif"<?php if($this->client->actif) echo " checked='checked'"; ?> /> <label for="actif">Activer le compte</label>
                    </p>
                </div>
            </section>

            <p id="p-chps-obligatoires">
                * Champs obligatoires
            </p>
            
            <p id="p-submit">
                <input type="submit" value="Valider" tabindex="6" />
                <input type="hidden" name="token" value="<?php echo \core\Securite::getToken(); ?>" />
            </p> 
        </form>

    </div>
</div>