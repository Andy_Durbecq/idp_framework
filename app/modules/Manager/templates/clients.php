<div id="wrap-container">
    <div id="page-clients">

        <h1>Clients</h1> 

        <div id="barre-outils">
            <div id="barre-outils-actions">
                <a href="<?php echo $this->application->getUrl(); ?>/manager/clients/ajouter-un-client/retour-page-<?php echo $this->page; ?>/tri-<?php echo $this->sens_tri; ?>-<?php echo $this->champ_tri; ?>"><i class="fa fa-fw fa-user-plus"></i>Ajouter un client</a>
            </div>
        </div>

        <section>
            <div class="section-container">
                <h2>Liste de tous les clients &nbsp;(<?php echo $this->total; ?>)</h2>

                <table>
                    <tbody>
                        <tr>
                            <th>ID <span class="tri"><a href="<?php echo $this->application->getUrl(); ?>/manager/clients/page-<?php echo $this->page; ?>/tri-desc-id"<?php if($this->sens_tri == "desc" && $this->champ_tri == "id") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-up"></i></a><a href="<?php echo $this->application->getUrl(); ?>/manager/clients/page-<?php echo $this->page; ?>/tri-asc-id"<?php if($this->sens_tri == "asc" && $this->champ_tri == "id") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-down"></i></a></span></th>                            
                            <th>Nom <span class="tri"><a href="<?php echo $this->application->getUrl(); ?>/manager/clients/page-<?php echo $this->page; ?>/tri-desc-nom"<?php if($this->sens_tri == "desc" && $this->champ_tri == "nom") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-up"></i></a><a href="<?php echo $this->application->getUrl(); ?>/manager/clients/page-<?php echo $this->page; ?>/tri-asc-nom"<?php if($this->sens_tri == "asc" && $this->champ_tri == "nom") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-down"></i></a></span></th>
                            <th>Email <span class="tri"><a href="<?php echo $this->application->getUrl(); ?>/manager/clients/page-<?php echo $this->page; ?>/tri-desc-email"<?php if($this->sens_tri == "desc" && $this->champ_tri == "email") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-up"></i></a><a href="<?php echo $this->application->getUrl(); ?>/manager/clients/page-<?php echo $this->page; ?>/tri-asc-email"<?php if($this->sens_tri == "asc" && $this->champ_tri == "email") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-down"></i></a></span></th>
                            <th>Actif <span class="tri"><a href="<?php echo $this->application->getUrl(); ?>/manager/clients/page-<?php echo $this->page; ?>/tri-desc-actif"<?php if($this->sens_tri == "desc" && $this->champ_tri == "actif") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-up"></i></a><a href="<?php echo $this->application->getUrl(); ?>/manager/clients/page-<?php echo $this->page; ?>/tri-asc-actif"<?php if($this->sens_tri == "asc" && $this->champ_tri == "actif") { echo " class='actif'"; } ?>><i class="fa fa-fw fa-caret-down"></i></a></span></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <?php
                        foreach ($this->clients as $client) {
                            echo "<tr>";
                            echo "<td class='center'>" . $client->id . "</td>";                            
                            echo "<td>" . $client->nom . " " . $client->prenom . "</td>";
                            echo "<td>" . $client->email . "</td>";
                            echo "<td class='center'>" . (($client->actif) ? "<i class='fa fa-fw fa-check' style='color: #00c921;'></i>" : "<i class='fa fa-fw fa-close' style='color: #ff0000;'></i>") . "</td>";
                            echo "<td class='center'><a href='" . $this->application->getUrl() . "/manager/clients/modifier-un-client/" . $client->id . "/retour-page-" . $this->page . "/tri-" . $this->sens_tri . "-" . $this->champ_tri . "' title='Modifier le client'><i class='fa fa-fw fa-pencil-square-o'></i></a></td>";
                            echo "<td class='center'><a href='" . $this->application->getUrl() . "/manager/clients/supprimer-un-client/" . $client->id . "/retour-page-" . $this->page . "/tri-" . $this->sens_tri . "-" . $this->champ_tri . "' title='Supprimer le client' onclick=\"return confirm('Voulez-vous vraiment supprimer cet client ?');\"><i class='fa fa-fw fa-trash-o'></i></a></td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>                       
                </table>
            </div>
        </section>

        <div id="pagination">
            <?php
            $debut_pagination = ($this->page - 5 >= 1) ? $this->page - 5 : 1;
            $fin_pagination = ($this->page + 5 <= ceil($this->total / $this->limite_par_page)) ? $this->page + 5 : ceil($this->total / $this->limite_par_page);
            for ($i = $debut_pagination; $i <= $fin_pagination; $i++) {
                if ($i == $this->page) {
                    echo "<a href='" . $this->application->getUrl() . "/manager/clients/page-" . $i . "/tri-" . $this->sens_tri . "-" . $this->champ_tri . "' class='actif'>" . $i . "</a>";
                } else {
                    echo "<a href='" . $this->application->getUrl() . "/manager/clients/page-" . $i . "/tri-" . $this->sens_tri . "-" . $this->champ_tri . "'>" . $i . "</a>";
                }
            }
            ?>
        </div>

    </div>
</div>