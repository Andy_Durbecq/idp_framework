<div id="wrap-container">
    <div id="page-tableau-de-bord">

        <h1>Tableau de bord</h1>   

        <form action="<?php echo $this->application->getUrl(); ?>/manager" method="post" autocomplete="off">
            <section>
                <div class="section-container">
                    <h2>Nous contacter - Déclarer un incident</h2>

                    <p>
                        <label for="nom">Nom</label>
                        <span>
                            <input type="text" name="nom" tabindex="1" autocomplete="off" id="nom" value="<?php
                            if (!empty($_POST['nom']))
                                echo htmlspecialchars($_POST['nom']);
                            else
                                echo htmlspecialchars($this->manager->nom);
                            ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="prenom">Prénom</label>
                        <span>
                            <input type="text" name="prenom" tabindex="2" autocomplete="off" id="prenom" value="<?php
                            if (!empty($_POST['prenom']))
                                echo htmlspecialchars($_POST['prenom']);
                            else
                                echo htmlspecialchars($this->manager->prenom);
                            ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="telephone">Téléphone *</label>
                        <span>
                            <input type="text" name="telephone" tabindex="3" autocomplete="off" id="telephone" value="<?php
                            if (!empty($_POST['telephone']))
                                echo htmlspecialchars($_POST['telephone']);
                            else
                                echo htmlspecialchars($this->manager->telephone);
                            ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="email">E-mail *</label>
                        <span>
                            <input type="text" name="email" tabindex="4" autocomplete="off" id="email" value="<?php
                                   if (!empty($_POST['email']))
                                       echo htmlspecialchars($_POST['email']);
                                   else
                                       echo htmlspecialchars($this->manager->email);
                                   ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="demande">Votre demande *</label>
                        <span>
                            <textarea id="demande" name="demande" tabindex="5" cols="5" rows="5"><?php if (!empty($_POST['demande'])) echo $_POST['demande']; ?></textarea>
                        </span>
                    </p>
                </div>
            </section>

            <p id="p-chps-obligatoires">
                * Champs obligatoires
            </p>

            <p id="p-submit">
                <input type="submit" value="Valider" tabindex="6" />
                <input type="hidden" name="token" value="<?php echo \core\Securite::getToken(); ?>" />
            </p> 
        </form>

    </div>
</div>