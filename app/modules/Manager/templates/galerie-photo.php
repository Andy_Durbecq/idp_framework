<div id="loader-ordre">
    <div>
        Tri en cours ...
    </div>
</div>

<div id="wrap-container">
    <div id="page-galerie">

        <h1>Gestion des galeries photo</h1> 

        <form action="<?php echo $this->application->getUrl(); ?>/manager/galerie-photo" method="post" autocomplete="off">
            <section>
                <div class="section-container">
                    <h2>Ajouter une galerie</h2>

                    <p>
                        <label for="nom">Nom de la galerie *</label>
                        <span>
                            <input type="text" name="nom" tabindex="1" autocomplete="off" id="nom" value="<?php if (!empty($_POST['nom'])) echo htmlspecialchars($_POST['nom']); ?>" />
                        </span>
                    </p>     
                </div>
            </section>

            <p id="p-chps-obligatoires">
                * Champs obligatoires
            </p>

            <p id="p-submit">
                <input type="submit" value="Valider" tabindex="2" />
                <input type="hidden" name="token" value="<?php echo \core\Securite::getToken(); ?>" />
            </p> 
        </form>

        <section style="margin-top: 25px;">
            <div class="section-container">
                <h2>Galerie(s) photo</h2>

                <i>Pour modifier l'ordre des galeries, un simple glisser déposer suffit !<br /><br /></i>

                <?php
                echo "<ul class='sortable-galerie'>";
                foreach ($this->galeries as $galerie) {
                    echo "<li data-id=\"" . $galerie->id . "\">";
                    echo "<div class=\"sortable-galerie-nom\"><input type=\"text\" name=\"nom_galerie_" . $galerie->id . "\" value=\"" . $galerie->nom . "\" /></div>";
                    echo "<div class=\"sortable-galerie-modifier\"><a href=\"#\" title=\"Modifier la galerie\"><i class='fa fa-fw fa-pencil-square-o'></i></a></div>";
                    echo "<div class=\"sortable-galerie-select\"><a href=\"#\" title=\"Afficher les photos de la galerie\"><i class=\"fa fa-fw fa-picture-o\"></i></a></div>";
                    echo "<div class=\"sortable-galerie-ordre\"><a href=\"#\" onclick=\"return false;\" title=\"Modifier l'ordre de la galerie par glisser-déposer\"><i class=\"fa fa-fw fa-sort\"></i></a></div>";                    
                    echo "<div class=\"sortable-galerie-supprimer\"><a href=\"" . $this->application->getUrl() . "/manager/galerie-photo/supprimer/" . $galerie->id . "\" title=\"Supprimer la galerie\" onclick=\"return confirm('Voulez-vous vraiment supprimer cette galerie ?');\"><i class='fa fa-fw fa-trash-o'></i></a></div>";
                    echo "</li>";
                }
                echo "</ul>";
                ?>  
            </div>
        </section>

        <section style="margin-top: 25px;">
            <div class="section-container">
                <h2>Photo(s) de la galerie</h2>

                <div id="upload-container">
                    <div>
                        Déposez vos fichiers ici (2Mo maximum / jpg) <br /><br /> ou
                    </div>
                    <a id="upload-browse" href="javascript:;">Sélectionner des fichiers</a>         
                </div>
                
                <div id="upload-console"></div>
                
                <div id="galerie-filelist"></div>

            </div>
        </section>

    </div>
</div>