<div id="loader-ordre">
    <div>
        Tri en cours ...
    </div>
</div>

<script type="text/javascript" src="<?php echo $this->application->config->url; ?>/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        plugins: "wordcount autolink link advlist lists fullscreen textcolor",
        theme: "modern",
        /*force_p_newlines : false,
         forced_root_block : false,*/
        toolbar: "undo redo | bold italic underline | link | bullist numlist outdent indent | forecolor | fullscreen ",
        menubar: false,
        statusbar: true,
        resize: false,
        language: 'fr_FR'
    });
</script>

<div id="wrap-container">
    <div id="page-clients">

        <h1>Modifier une actualité</h1> 

        <div id="barre-outils">
            <div id="barre-outils-form">
                <a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/<?php echo $this->datas['retour_url']; ?>"><i class="fa fa-fw fa-backward"></i>Retour à la liste</a>
            </div>
        </div>

        <form action="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/modifier-un-post/<?php echo $this->post->id; ?>/retour-<?php echo $this->datas['retour_url']; ?>" method="post" autocomplete="off">
            <section>
                <div class="section-container">
                    <p>
                        <label for="date">Date *</label>
                        <span>
                            <input type="text" name="date" class="datepicker" tabindex="1" autocomplete="off" id="date" value="<?php if (!empty($_POST['date'])) echo htmlspecialchars($_POST['date']); else echo date('d/m/Y', $this->post->date) ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="titre">Titre *</label>
                        <span>
                            <input type="text" name="titre" tabindex="2" autocomplete="off" id="titre" value="<?php if (!empty($_POST['titre'])) echo htmlspecialchars($_POST['titre']); else echo htmlspecialchars($this->post->titre); ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="sous-titre">Sous titre</label>
                        <span>
                            <input type="text" name="sous_titre" tabindex="3" autocomplete="off" id="sous-titre" value="<?php if (!empty($_POST['sous_titre'])) echo htmlspecialchars($_POST['sous_titre']); else echo htmlspecialchars($this->post->sous_titre); ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="texte">Texte *<br /><i>Un appui sur la touche "entrée" donnera un nouveau paragraphe. <br />Pour un simple saut de ligne, il faut maintenair "shift" et appuyer sur "entrée"<br /><br /><b>ATTENTION :</b> le copier/coller de texte n'est pas recommandé. Si vous souhaitez copier un contenu d'une source, copiez d'abord celle-ci dans un "document texte windows : .txt" (Pas de word) afin d'éviter tout problème de mise en page.</i></label>
                        <span>
                            <textarea id="texte" name="texte" tabindex="4" cols="5" rows="5"><?php if (!empty($_POST['texte'])) echo $_POST['texte']; else echo $this->post->texte; ?></textarea>
                        </span>
                    </p>
                    <p>
                        <label for="ordre">Ordre</label>
                        <span>
                            <input type="text" name="ordre" tabindex="5" autocomplete="off" id="ordre" value="<?php if (!empty($_POST['ordre'])) echo htmlspecialchars($_POST['ordre']); else echo htmlspecialchars($this->post->ordre); ?>" />
                        </span>
                    </p>    
                    <p class="p-checkbox-radio">
                        <input type="checkbox" value="1" tabindex="6" name="actif" id="actif"<?php if ($this->post->actif) echo " checked='checked'"; ?> /> <label for="actif">Rendre visible</label>
                    </p>
                </div>
            </section>

            <p id="p-chps-obligatoires">
                * Champs obligatoires
            </p>

            <p id="p-submit" style="margin-bottom: 25px;">
                <input type="submit" value="Valider" tabindex="6" />
                <input type="hidden" name="token" value="<?php echo \core\Securite::getToken(); ?>" />
                <input type="hidden" name="action" value="update" />
            </p> 
        </form>

        <form action="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/modifier-un-post/<?php echo $this->post->id; ?>/retour-<?php echo $this->datas['retour_url']; ?>" method="post" autocomplete="off" enctype="multipart/form-data">
            <section>
                <div class="section-container">
                    <h2>Images</h2>

                    <p>
                        <label for="fichier">Image (jpg et 4Mo maximum)</label>
                        <span>
                            <input type="file" name="fichier" id="fichier" />
                        </span>
                    </p>
                    <p>
                        <label for="legende">Légende</label>
                        <span>
                            <input type="text" name="legende" autocomplete="off" id="legende" value="<?php if (!empty($_POST['legende'])) echo htmlspecialchars($_POST['legende']); ?>" />
                        </span>
                    </p>                    
                    <p id="p-submit" style="margin-bottom: 25px;">
                        <input type="submit" value="Envoyer l'image" />
                        <input type="hidden" name="token" value="<?php echo \core\Securite::getToken(); ?>" />
                        <input type="hidden" name="action" value="add_image" />
                    </p> 

                    <i>Pour modifier l'ordre des visuels, un simple glisser déposer suffit !<br /><br /></i>

                    <div class="liste-doc">
                        <?php
                        if (!empty($this->post_images)) {
                            echo "<ul class='sortable'>";
                            foreach ($this->post_images as $post_image) {
                                echo "<li data-id='" . $post_image->id . "'>";
                                echo "<span class='liste-doc-ordre'>" . $post_image->ordre . "</span>";
                                echo "<img src='" . $this->application->config->url . "/downloads/posts/thumb-" . $post_image->fichier . "' alt='' />";
                                echo "<span>" . $post_image->legende . "</span>";
                                echo "<span class='liste-doc-delete'><a href='" . $this->application->getUrl() . "/manager/posts-" . $this->datas['type'] . "/supprimer-un-post-image/" . $this->post->id . "/" . $post_image->id . "/retour-" . $this->datas['retour_url'] . "' title=\"Supprimer l'image\" onclick=\"return confirm('Supprimer cette image ?');\"><i class='fa fa-fw fa-trash-o'></i></a></span>";
                                echo "</li>";
                            }
                            echo "</ul>";
                        }
                        ?>
                    </div>
                </div>
            </section>            
        </form>

        <form action="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/modifier-un-post/<?php echo $this->post->id; ?>/retour-<?php echo $this->datas['retour_url']; ?>" method="post" autocomplete="off" enctype="multipart/form-data">
            <section>
                <div class="section-container">
                    <h2>Documents</h2>

                    <p>
                        <label for="fichier">Pdf (pdf et 4Mo maximum)</label>
                        <span>
                            <input type="file" name="fichier" id="fichier" />
                        </span>
                    </p>
                    <p>
                        <label for="legende">Légende</label>
                        <span>
                            <input type="text" name="legende" autocomplete="off" id="legende" value="<?php if (!empty($_POST['legende'])) echo htmlspecialchars($_POST['legende']); ?>" />
                        </span>
                    </p>                    
                    <p id="p-submit" style="margin-bottom: 25px;">
                        <input type="submit" value="Envoyer le PDF" />
                        <input type="hidden" name="token" value="<?php echo \core\Securite::getToken(); ?>" />
                        <input type="hidden" name="action" value="add_document" />
                    </p> 

                    <div class="liste-doc">
                        <?php
                        if (!empty($this->post_documents)) {
                            echo "<ul id='ul-doc'>";
                            foreach ($this->post_documents as $post_document) {
                                echo "<li>";
                                echo "<span class='liste-doc-ordre'>" . $post_document->ordre . "</span>";
                                echo "<span>" . $post_document->fichier . "</span>";
                                echo "<span class='liste-doc-delete'><a href='" . $this->application->getUrl() . "/manager/posts-" . $this->datas['type'] . "/supprimer-un-post-document/" . $this->post->id . "/" . $post_document->id . "/retour-" . $this->datas['retour_url'] . "' title=\"Supprimer le document\" onclick=\"return confirm('Supprimer ce document ?');\"><i class='fa fa-fw fa-trash-o'></i></a></span>";
                                echo "</li>";
                            }
                            echo "</ul>";
                        }
                        ?>
                    </div>
                </div>
            </section>
        </form>

    </div>
</div>