<div id="wrap-container">

    <h1>Erreur 404 !</h1>

    <section>
        <div class="section-container">
            <h2>Page introuvable !</h2>
            
            <p>
                La page que vous demandez n'existe pas ou n'existe plus !<br /><br />
            </p>
            
            <a href="<?php echo $this->application->getUrl(); ?>/manager">&gt; Retourner à l'accueil</a>
        </div>
    </section>

</div>
