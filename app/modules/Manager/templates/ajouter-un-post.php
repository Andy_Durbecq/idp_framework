<script type="text/javascript" src="<?php echo $this->application->config->url; ?>/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        plugins: "wordcount autolink link advlist lists fullscreen textcolor",
        theme: "modern",
        /*force_p_newlines : false,
        forced_root_block : false,*/
        toolbar: "undo redo | bold italic underline | link | bullist numlist outdent indent | forecolor | fullscreen ",
        menubar : false,
        statusbar : true,
        resize: false,
        language: 'fr_FR'
     });
</script>

<div id="wrap-container">
    <div id="page-clients">

        <h1>Ajouter une actualité</h1> 

        <div id="barre-outils">
            <div id="barre-outils-form">
                <a href="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/<?php echo $this->datas['retour_url']; ?>"><i class="fa fa-fw fa-backward"></i>Retour à la liste</a>
            </div>
        </div>

        <form action="<?php echo $this->application->getUrl(); ?>/manager/posts-<?php echo $this->datas['type']; ?>/ajouter-un-post/retour-<?php echo $this->datas['retour_url']; ?>" method="post" autocomplete="off">
            <section>
                <div class="section-container">
                    <p>
                        <label for="date">Date *</label>
                        <span>
                            <input type="text" name="date" class="datepicker" tabindex="1" autocomplete="off" id="date" value="<?php if (!empty($_POST['date'])) echo htmlspecialchars($_POST['date']); ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="titre">Titre *</label>
                        <span>
                            <input type="text" name="titre" tabindex="2" autocomplete="off" id="titre" value="<?php if (!empty($_POST['titre'])) echo htmlspecialchars($_POST['titre']); ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="sous-titre">Sous titre</label>
                        <span>
                            <input type="text" name="sous_titre" tabindex="3" autocomplete="off" id="sous-titre" value="<?php if (!empty($_POST['sous_titre'])) echo htmlspecialchars($_POST['sous_titre']); ?>" />
                        </span>
                    </p>
                    <p>
                        <label for="texte">Texte *<br /><i>Un appui sur la touche "entrée" donnera un nouveau paragraphe. <br />Pour un simple saut de ligne, il faut maintenair "shift" et appuyer sur "entrée"<br /><br /><b>ATTENTION :</b> le copier/coller de texte n'est pas recommandé. Si vous souhaitez copier un contenu d'une source, copiez d'abord celle-ci dans un "document texte windows : .txt" (Pas de word) afin d'éviter tout problème de mise en page.</i></label>
                        <span>
                            <textarea id="texte" name="texte" tabindex="4" cols="5" rows="5"><?php if (!empty($_POST['texte'])) echo $_POST['texte']; ?></textarea>
                        </span>
                    </p>
                    <p>
                        <label for="ordre">Ordre</label>
                        <span>
                            <input type="text" name="ordre" tabindex="5" autocomplete="off" id="ordre" value="<?php if (!empty($_POST['ordre'])) echo htmlspecialchars($_POST['ordre']); else echo "1"; ?>" />
                        </span>
                    </p>    
                    <p class="p-checkbox-radio">
                        <input type="checkbox" value="1" tabindex="6" name="actif" id="actif" /> <label for="actif">Rendre visible</label>
                    </p>
                </div>
            </section>

            <p id="p-chps-obligatoires">
                * Champs obligatoires
            </p>

            <p id="p-submit" style="margin-bottom: 25px;">
                <input type="submit" value="Valider" tabindex="7" />
                <input type="hidden" name="token" value="<?php echo \core\Securite::getToken(); ?>" />
            </p> 
        </form>

        <section>
            <div class="section-container">
                <h2>Images</h2>
                
                <p>
                    <i>
                        Afin de pouvoir ajouter/supprimer des images merci d'enregistrer une première fois.
                    </i>
                </p>
            </div>
        </section>
        
        <section>
            <div class="section-container">
                <h2>Documents</h2>
                
                <p>
                    <i>
                        Afin de pouvoir ajouter/supprimer des documents merci d'enregistrer une première fois.
                    </i>
                </p>
            </div>
        </section>

    </div>
</div>