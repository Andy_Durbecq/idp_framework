<?php

namespace app\modules\Manager;

use slugifier;

class Manager extends \core\Module
{

    public $manager;
    public $logs;
    public $manager_manager;
    public $manager_internaute;
    public $manager_post;
    public $manager_log_manager;
    public $manager_galerie;
    public $manager_ticket;
    public $managers;
    public $manager_tmp;
    public $clients;
    public $client;
    public $galeries;
    public $posts;
    public $post;
    public $total;
    public $limite_par_page = 20;
    public $page = 1;
    public $sens_tri = "asc";
    public $champ_tri;
    public $max = 1;
    public $post_images;
    public $post_documents;
    public $tickets;

    public function __construct(\core\Application $application, $datas = array())
    {
        parent::__construct($application, $datas);

        // Différents modèles utilie dans le manager
        $this->manager_manager = new \models\ManagerManager(\core\Application::$bdd);
        $this->manager = $this->manager_manager->checkConnexion();

        $this->manager_internaute = new \models\InternauteManager(\core\Application::$bdd);
        $this->manager_post = new \models\PostManager(\core\Application::$bdd);
        $this->manager_log_manager = new \models\Log_managerManager(\core\Application::$bdd);
        $this->manager_galerie = new \models\GalerieManager(\core\Application::$bdd);
        $this->manager_ticket = new \models\TicketManager(\core\Application::$bdd);
    }

    private function estConnecte()
    {
        if (!$this->manager) {
            $this->application->redirection("manager/connexion");
        }
    }

    // ---------- Connexion et déconnexion ---------- //
    public function formConnexionAction()
    {
        if ($_POST) {
            if (!\core\Securite::verifToken($_POST['token'])) {
                trigger_error("External connection to the unauthorized manager via an invalid token!", E_USER_ERROR);
                exit();
            }
            if (empty($_POST['email']) || !\core\Validate::email($_POST['email']) || empty($_POST['pass'])) {
                $_SESSION['message_d_erreur'] = "E-mail ou mot de passe incorrect !";
            }
            if (!$this->application->error_message) {
                if ($this->manager_manager->connexion($_POST['email'], \core\Securite::hashWithToken($_POST['pass'], $this->application->config->hash_key))) {
                    // Log de la connexion
                    $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $_POST['email'], "action" => "Connexion au manager via cet email")));

                    $this->application->redirection("manager");
                } else {
                    $_SESSION['message_d_erreur'] = "E-mail ou mot de passe incorrect !";
                }
            }
        }

        // Verification d'une précédente connexion
        if ($this->manager) {
            $this->application->redirection("manager");
        }

        $this->template("manager-header.php");
        $this->template("formulaire-connexion.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function deconnexionAction()
    {
        $this->manager_manager->deconnexion();
        $this->application->redirection("manager");
    }

    // ---------- Accueil et cache ---------- //
    public function indexAction()
    {
        $this->estConnecte();

        if ($_POST) {
            $erreur_message = "";
            if (empty($_POST['telephone'])) {
                $erreur_message .= "<br /> - Merci de renseigner un numéro de téléphone";
            }
            if (empty($_POST['email'])) {
                $erreur_message .= "<br /> - Merci de renseigner un e-mail";
            }
            if (empty($_POST['demande'])) {
                $erreur_message .= "<br /> - Merci de renseigner une demande";
            }
            if (!empty($_POST['telephone']) && !\core\Validate::phone($_POST['telephone'])) {
                $erreur_message .= "<br /> - Merci de renseigner un numéro de téléphone valide";
            }
            if (!empty($_POST['email']) && !\core\Validate::email($_POST['email'])) {
                $erreur_message .= "<br /> - Merci de renseigner un e-mail valide";
            }

            if (empty($erreur_message)) {
                $html = file_get_contents(__DIR__ . "/../../../mails/ticket.php");
                utf8_encode($html);
                $html = str_replace(array("{nom_du_site}", "{prenom}", "{nom}", "{telephone}", "{email}", "{demande}"), array($this->application->config->nom, $_POST['prenom'], $_POST['nom'], $_POST['telephone'], $_POST['email'], nl2br($_POST['demande'])), $html);
                $mailer = new \core\Mailer();
                $mailer->set_from($_POST['email']);
                $mailer->set_address("ticket@id-parallele.com");
                $mailer->set_format('html');
                $mailer->set_subject('Déclaration d\'un incident');
                $mailer->set_message(stripslashes($html));
                $mailer->send();

                /* $message = \Swift_Message::newInstance()
                  ->setSubject('Déclaration d\'un incident')
                  ->setFrom($_POST['email'])
                  ->setTo('ticket@id-parallele.com')
                  ->setBody($html, 'text/html');
                  $this->application->swiftmailer->send($message); */

                $ticket = new \models\Ticket(array(
                    "nom" => $_POST['nom'],
                    "prenom" => $_POST['prenom'],
                    "telephone" => $_POST['telephone'],
                    "email" => $_POST['email'],
                    "demande" => $_POST['demande'],
                    "date" => time(),
                ));

                $this->manager_ticket->insert($ticket);

                $_SESSION['message_info'] = "Votre demande a bien été enregistrée. Vous allez être recontacter dès que possible.";

                $this->application->redirection("manager");
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $erreur_message;
            }
        }

        $this->manager_log_manager->delete();

        $this->datas['menu_actif'] = 1;

        $this->template("manager-header.php");
        $this->template("accueil.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function viderLeCacheAction()
    {
        $this->estConnecte();

        $this->application->viderLeCache();

        $_SESSION['message_info'] = "Le cache a bien été vidé.";

        $this->application->redirection("manager");
    }

    // ---------- Logs ----------//
    public function logsAction()
    {
        $this->estConnecte();

        $limit = (!empty($_GET['limit']) && is_numeric($_GET['limit'])) ? $_GET['limit'] : 100;

        $this->logs = $this->manager_log_manager->getAll($limit);

        $this->datas['menu_actif'] = 5;

        $this->template("manager-header.php");
        $this->template("logs.php", "Manager");
        $this->template("manager-footer.php");
    }

    // ---------- Managers ---------- //
    public function managersAction()
    {
        $this->estConnecte();

        $this->total = $this->manager_manager->total();
        $this->page = (!empty($this->datas['page']) && $this->datas['page'] <= ceil($this->total / $this->limite_par_page)) ? $this->datas['page'] : 1;

        if (!empty($this->datas['sens_tri'])) {
            $this->sens_tri = $this->datas['sens_tri'];
        }
        if (!empty($this->datas['champ_tri'])) {
            $this->champ_tri = $this->datas['champ_tri'];
        } else {
            $this->champ_tri = "id";
        }

        $this->managers = $this->manager_manager->getPage($this->page, $this->limite_par_page, "", $this->champ_tri, $this->sens_tri);

        $this->datas['menu_actif'] = 4;

        $this->template("manager-header.php");
        $this->template("managers.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function ajouterUnManagerAction()
    {
        $this->estConnecte();

        if ($_POST) {
            if (!\core\Securite::verifToken($_POST['token'])) {
                trigger_error("Ajout d'un nouveau manager via un token invalide !", E_USER_ERROR);
                exit();
            }

            $message_d_erreur = "";
            if (empty($_POST['nom'])) {
                $message_d_erreur .= "<br /> - Un nom est obligatoire";
            }
            if (empty($_POST['prenom'])) {
                $message_d_erreur .= "<br /> - Un prénom est obligatoire";
            }
            if (empty($_POST['email']) || (!empty($_POST['email']) && !\core\Validate::email($_POST['email']))) {
                $message_d_erreur .= "<br /> - Un E-mail valide est obligatoire";
            }
            if (empty($_POST['pass'])) {
                $message_d_erreur .= "<br /> - Un mot de passe est obligatoire";
            }

            if (empty($message_d_erreur)) {
                // On vérifie l'existance d'un potentiel manager avec cette email
                $manager = $this->manager_manager->getEmail($_POST['email']);
                if ($manager) {
                    $_SESSION['message_d_erreur'] = "Cette e-mail est déjà utilisé";
                } else {
                    $actif = (!empty($_POST['actif'])) ? 1 : 0;

                    $this->manager_tmp = new \models\Manager(array(
                        "nom" => $_POST['nom'],
                        "prenom" => $_POST['prenom'],
                        "email" => $_POST['email'],
                        "pass" => \core\Securite::hashWithToken($_POST['pass'], $this->application->config->hash_key),
                        "statut" => $actif,
                        "telephone" => "",
                        "portable" => "",
                        "description" => ""
                    ));

                    $this->manager_manager->insert($this->manager_tmp);

                    // Log ajout manager
                    $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Ajout d'un nouveau manager -> " . htmlspecialchars($this->manager_tmp->nom . " " . $this->manager_tmp->prenom))));

                    $_SESSION['message_info'] = "Manager ajouté";

                    $this->application->redirection("manager/managers/" . $this->datas['retour_url']);
                }
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $message_d_erreur;
            }
        }

        $this->datas['menu_actif'] = 4;

        $this->template("manager-header.php");
        $this->template("ajouter-un-manager.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function modifierUnManagerAction()
    {
        $this->estConnecte();

        $this->manager_tmp = $this->manager_manager->get($this->datas['id_manager']);
        if (!$this->manager_tmp || $this->manager_tmp->id == 1) {
            $_SESSION['message_d_erreur'] = "Le manager demandé n'existe pas";
            $this->application->redirection("manager/managers/" . $this->datas['retour_url']);
        }

        if ($_POST) {
            if (!\core\Securite::verifToken($_POST['token'])) {
                trigger_error("Modification d'un manager via un token invalide !", E_USER_ERROR);
                exit();
            }

            $message_d_erreur = "";
            if (empty($_POST['nom'])) {
                $message_d_erreur .= "<br /> - Un nom est obligatoire";
            }
            if (empty($_POST['prenom'])) {
                $message_d_erreur .= "<br /> - Un prénom est obligatoire";
            }
            if (empty($_POST['email']) || (!empty($_POST['email']) && !\core\Validate::email($_POST['email']))) {
                $message_d_erreur .= "<br /> - Un E-mail valide est obligatoire";
            }

            if (empty($message_d_erreur)) {
                // On vérifie l'existance d'un potentiel manager si l'email est différent de celui d'origine
                $manager = $this->manager_manager->getEmail($_POST['email']);
                if ($manager && $this->manager_tmp->email != $_POST['email']) {
                    $_SESSION['message_d_erreur'] = "Cette e-mail est déjà utilisé";
                } else {
                    $actif = (!empty($_POST['actif'])) ? 1 : 0;
                    $mot_de_passe = (!empty($_POST['pass'])) ? \core\Securite::hashWithToken($_POST['pass'], $this->application->config->hash_key) : $this->manager_tmp->pass;

                    $this->manager_tmp->hydrate(array(
                        "nom" => $_POST['nom'],
                        "prenom" => $_POST['prenom'],
                        "email" => $_POST['email'],
                        "pass" => $mot_de_passe,
                        "statut" => $actif,
                        "telephone" => "",
                        "portable" => "",
                        "description" => ""
                    ));

                    $this->manager_manager->update($this->manager_tmp);

                    // Log modification manager
                    $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Modification du manager -> " . htmlspecialchars($this->manager_tmp->nom . " " . $this->manager_tmp->prenom))));

                    $_SESSION['message_info'] = "Manager modifié";

                    $this->application->redirection("manager/managers/" . $this->datas['retour_url']);
                }
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $message_d_erreur;
            }
        }

        $this->datas['menu_actif'] = 2;

        $this->template("manager-header.php");
        $this->template("modifier-un-manager.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function supprimerUnmanagerAction()
    {
        $this->estConnecte();

        $this->manager_tmp = $this->manager_manager->get($this->datas['id_manager']);
        if (!$this->manager_tmp || $this->manager_tmp->id == 1) {
            $_SESSION['message_d_erreur'] = "Le manager demandé n'existe pas";
            $this->application->redirection("manager/managers/" . $this->datas['retour_url']);
        }

        $this->manager_manager->delete($this->manager_tmp->id);

        // Log modification manager
        $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Suppression du manager -> " . htmlspecialchars($this->manager_tmp->nom . " " . $this->manager_tmp->prenom))));

        $_SESSION['message_info'] = "Manager supprimé";

        $this->application->redirection("manager/managers/" . $this->datas['retour_url']);
    }

    // ---------- Clients ---------- //
    public function clientsAction()
    {
        $this->estConnecte();

        $this->total = $this->manager_internaute->total();
        $this->page = (!empty($this->datas['page']) && $this->datas['page'] <= ceil($this->total / $this->limite_par_page)) ? $this->datas['page'] : 1;

        if (!empty($this->datas['sens_tri'])) {
            $this->sens_tri = $this->datas['sens_tri'];
        }
        if (!empty($this->datas['champ_tri'])) {
            $this->champ_tri = $this->datas['champ_tri'];
        } else {
            $this->champ_tri = "id";
        }

        $this->clients = $this->manager_internaute->getPage($this->page, $this->limite_par_page, "", $this->champ_tri, $this->sens_tri);

        $this->datas['menu_actif'] = 2;

        $this->template("manager-header.php");
        $this->template("clients.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function ajouterUnClientAction()
    {
        $this->estConnecte();

        if ($_POST) {
            if (!\core\Securite::verifToken($_POST['token'])) {
                trigger_error("Ajout d'un nouvel internaute via un token invalide !", E_USER_ERROR);
                exit();
            }

            $message_d_erreur = "";
            if (empty($_POST['nom'])) {
                $message_d_erreur .= "<br /> - Un nom est obligatoire";
            }
            if (empty($_POST['prenom'])) {
                $message_d_erreur .= "<br /> - Un prénom est obligatoire";
            }
            if (empty($_POST['email']) || (!empty($_POST['email']) && !\core\Validate::email($_POST['email']))) {
                $message_d_erreur .= "<br /> - Un E-mail valide est obligatoire";
            }
            if (empty($_POST['pass'])) {
                $message_d_erreur .= "<br /> - Un mot de passe est obligatoire";
            }

            if (empty($message_d_erreur)) {
                // On vérifie l'existance d'un potentiel internaute avec cette email
                $internaute = $this->manager_internaute->getEmail($_POST['email']);
                if ($internaute) {
                    $_SESSION['message_d_erreur'] = "Cette e-mail est déjà utilisé";
                } else {
                    $actif = (!empty($_POST['actif'])) ? 1 : 0;

                    $this->client = new \models\Internaute(array(
                        "id_facebook" => "",
                        "token_facebook" => "",
                        "nom" => $_POST['nom'],
                        "prenom" => $_POST['prenom'],
                        "email" => $_POST['email'],
                        "pass" => \core\Securite::hashWithToken($_POST['pass'], $this->application->config->hash_key),
                        "actif" => $actif
                    ));

                    $this->manager_internaute->insert($this->client);

                    $this->application->viderLeCache();

                    $_SESSION['message_info'] = "Client ajouté";

                    // Log ajout d'un client
                    $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Ajout d'un nouveau client -> " . htmlspecialchars($this->client->nom . " " . $this->client->prenom))));

                    $this->application->redirection("manager/clients/" . $this->datas['retour_url']);
                }
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $message_d_erreur;
            }
        }

        $this->datas['menu_actif'] = 2;

        $this->template("manager-header.php");
        $this->template("ajouter-un-client.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function modifierUnClientAction()
    {
        $this->estConnecte();

        $this->client = $this->manager_internaute->get($this->datas['id_internaute']);
        if (!$this->client) {
            $_SESSION['message_d_erreur'] = "Le client demandé n'existe pas";
            $this->application->redirection("manager/clients/" . $this->datas['retour_url']);
        }

        if ($_POST) {
            if (!\core\Securite::verifToken($_POST['token'])) {
                trigger_error("Modification d'un internaute via un token invalide !", E_USER_ERROR);
                exit();
            }

            $message_d_erreur = "";
            if (empty($_POST['nom'])) {
                $message_d_erreur .= "<br /> - Un nom est obligatoire";
            }
            if (empty($_POST['prenom'])) {
                $message_d_erreur .= "<br /> - Un prénom est obligatoire";
            }
            if (empty($_POST['email']) || (!empty($_POST['email']) && !\core\Validate::email($_POST['email']))) {
                $message_d_erreur .= "<br /> - Un E-mail valide est obligatoire";
            }

            if (empty($message_d_erreur)) {
                // On vérifie l'existance d'un potentiel internaute si l'email est différent de celui d'origine
                $internaute = $this->manager_internaute->getEmail($_POST['email']);
                if ($internaute && $this->client->email != $_POST['email']) {
                    $_SESSION['message_d_erreur'] = "Cette e-mail est déjà utilisé";
                } else {
                    $actif = (!empty($_POST['actif'])) ? 1 : 0;
                    $mot_de_passe = (!empty($_POST['pass'])) ? \core\Securite::hashWithToken($_POST['pass'], $this->application->config->hash_key) : $this->client->pass;

                    $this->client->hydrate(array(
                        "nom" => $_POST['nom'],
                        "prenom" => $_POST['prenom'],
                        "email" => $_POST['email'],
                        "pass" => $mot_de_passe,
                        "actif" => $actif
                    ));

                    $this->manager_internaute->update($this->client);

                    $this->application->viderLeCache();

                    $_SESSION['message_info'] = "Client modifié";

                    // Log modification d'un client
                    $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Modification du client -> " . htmlspecialchars($this->client->nom . " " . $this->client->prenom))));

                    $this->application->redirection("manager/clients/" . $this->datas['retour_url']);
                }
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $message_d_erreur;
            }
        }

        $this->datas['menu_actif'] = 2;

        $this->template("manager-header.php");
        $this->template("modifier-un-client.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function supprimerUnClientAction()
    {
        $this->estConnecte();

        $this->client = $this->manager_internaute->get($this->datas['id_internaute']);
        if (!$this->client) {
            $_SESSION['message_d_erreur'] = "Le client demandée n'existe pas";
            $this->application->redirection("manager/clients/" . $this->datas['retour_url']);
        }

        $this->manager_internaute->delete($this->client->id);

        $this->application->viderLeCache();

        $_SESSION['message_info'] = "Client supprimé";

        // Log modification d'un client
        $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Suppression du client -> " . htmlspecialchars($this->client->nom . " " . $this->client->prenom))));

        $this->application->redirection("manager/clients/" . $this->datas['retour_url']);
    }

    // ---------- Galerie ---------- //
    public function galeriePhotoAction()
    {
        $this->estConnecte();

        if ($_POST) {
            if (!\core\Securite::verifToken($_POST['token'])) {
                trigger_error("Ajout d'une nouvelle galerie via un token invalide !", E_USER_ERROR);
                exit();
            }

            $message_d_erreur = "";
            if (empty($_POST['nom'])) {
                $message_d_erreur .= "<br /> - Un nom est obligatoire";
            }

            if (empty($message_d_erreur)) {
                $galerie = new \models\Galerie(array(
                    "nom" => $_POST['nom'],
                    "ordre" => 1
                ));

                $this->manager_galerie->remiseOrdre();

                $this->manager_galerie->insert($galerie);

                $this->application->viderLeCache();

                // Log ajout d'une galerie
                $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Ajout d'une nouvelle galerie -> " . htmlspecialchars($_POST['nom']))));

                $_SESSION['message_info'] = "Nouvelle galerie ajoutée";

                $this->application->redirection("manager/galerie-photo");
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $message_d_erreur;
            }
        }

        $this->galeries = $this->manager_galerie->getAll();

        $this->datas['menu_actif'] = 6;

        $this->css[] = "jquery-ui.min.css";
        $this->javascript[] = "jquery-ui.min.js";

        $this->javascript[] = "plupload.full.min.js";
        $this->javascript[] = "fr-plupload.js";

        $this->template("manager-header.php");
        $this->template("galerie-photo.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function supprimerGaleriePhotoAction()
    {
        $this->estConnecte();

        if (!empty($this->datas['id_galerie']) && is_numeric($this->datas['id_galerie'])) {
            $galerie = $this->manager_galerie->get($this->datas['id_galerie']);
            if ($galerie) {
                $photos = $this->manager_galerie->getAllPhoto($galerie->id);
                foreach ($photos as $photo) {
                    unlink(__DIR__ . "/../../../web/downloads/gallery/thumb-" . $photo->fichier);
                    unlink(__DIR__ . "/../../../web/downloads/gallery/" . $photo->fichier);

                    $this->manager_galerie->deletePhoto($photo->id);
                }

                $this->manager_galerie->delete($galerie->id);

                $this->application->viderLeCache();

                // Log ajout d'une galerie
                $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Suppression de la galerie -> " . htmlspecialchars($galerie->nom))));

                $_SESSION['message_info'] = "Galerie supprimée";
            }
        }

        $this->application->redirection("manager/galerie-photo");
    }

    public function modifierGaleriePhotoAction()
    {
        $this->estConnecte();

        header('Content-Type: application/json');
        $retour = new \stdClass();

        if (!empty($_POST['id_galerie']) && is_numeric($_POST['id_galerie'])) {
            $galerie = $this->manager_galerie->get($_POST['id_galerie']);
            if ($galerie) {
                if (!empty($_POST['ordre']) && is_numeric($_POST['ordre'])) {
                    $galerie->ordre = $_POST['ordre'];

                    $this->manager_galerie->update($galerie);
                } elseif (!empty($_POST['nom'])) {
                    $galerie->nom = $_POST['nom'];

                    $this->manager_galerie->update($galerie);
                }
            }
        }

        $this->application->viderLeCache();

        echo json_encode($retour);
    }

    public function ajouterPhotoAction()
    {
        $this->estConnecte();

        // Pas de cache
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        // Time limit 10 minutes
        @set_time_limit(10 * 60);

        // Id de la galerie
        if (!empty($_POST['id_galerie']) && is_numeric($_POST['id_galerie'])) {
            $id_galerie = $_POST['id_galerie'];
        } else {
            die('{"error" : true, "message" : "Erreur lors de la récupération de la galerie !"}');
        }

        // Répertoire de destination
        $repertoire = __DIR__ . "/../../../web/downloads/gallery/";

        // Taille maximum du fichier (2Mo)
        $taille_maximum = 2097152;

        // Fichier a uploader (POST)
        $file = $_FILES['file'];

        // Check upload fichier reussie dans les temporaires
        if (!is_uploaded_file($file['tmp_name']) || (!empty($file['error']) && $file['error'] == 1)) {
            die('{"error" : true, "message" : "Erreur lors du téléchargement, veuillez réessayer !"}');
        }

        // Test la taille maximum du fichier
        if (filesize($file['tmp_name']) > $taille_maximum) {
            die('{"error" : true, "message" : "Votre fichier est trop lourd !"}');
        }

        // Type de fichier mime
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type_mime = finfo_file($finfo, $file['tmp_name']);
        finfo_close($finfo);

        // Verification extension autorisées
        if (!in_array($type_mime, array("image/jpeg"))) {
            die('{"error" : true, "message" : "Type de fichier non autorisé !"}');
        }

        // Upload du fichier
        $fichier = slugifier\slugify(pathinfo($file['name'], PATHINFO_FILENAME)) . "-" . rand(1, 999) . "." . pathinfo($file['name'], PATHINFO_EXTENSION);

        move_uploaded_file($file['tmp_name'], $repertoire . $fichier);

        copy($repertoire . $fichier, $repertoire . "thumb-" . $fichier);
        $image = new \Eventviva\ImageResize($repertoire . "thumb-" . $fichier);
        $image->crop(110, 110);
        $image->save($repertoire . "thumb-" . $fichier);

        $this->manager_galerie->remiseOrdrePhoto($id_galerie);

        $galerie_photo = new \models\Galerie_photo(array(
            "id_galerie" => $id_galerie,
            "legende" => "",
            "fichier" => $fichier,
            "ordre" => 1
        ));
        $this->manager_galerie->insertPhoto($galerie_photo);

        $id_galerie_photo = \core\Application::$bdd->lastInsertId();

        // Vignette à afficher
        $image = "thumb-" . $fichier;

        $this->application->viderLeCache();

        echo '{"error" : false, "fichier" : "' . $fichier . '", "id" : "' . $id_galerie_photo . '", "vignette" : "' . $image . '"}';
    }

    public function getPhotoAction()
    {
        $this->estConnecte();

        header('Content-Type: application/json');
        $retour = new \stdClass();
        $retour->html = "";

        if (!empty($_POST['id_galerie']) && is_numeric($_POST['id_galerie'])) {
            $galerie = $this->manager_galerie->get($_POST['id_galerie']);
            if ($galerie) {
                $photos = $this->manager_galerie->getAllPhoto($galerie->id);

                foreach ($photos as $photo) {
                    $retour->html .= '<div id="photo-' . $photo->id . '" data-id="' . $photo->id . '" class="galerie-filelist-photo galerie-filelist-photo-sortable">';
                    $retour->html .= '<div class="galerie-filelist-photo-img"><img src="/downloads/gallery/thumb-' . $photo->fichier . '" alt="" /></div>';
                    $retour->html .= '<div class="galerie-filelist-photo-input"><input type="text" name="nom_photo_' . $photo->id . '" value="' . $photo->legende . '" /></div>';
                    $retour->html .= '<div class="galerie-filelist-photo-modifier"><a href="#" title="Modifier la légende la photo"><i class="fa fa-fw fa-pencil-square-o"></i></a></div>';
                    $retour->html .= '<div class="galerie-filelist-photo-ordre"><a href="#" title="Modifier l\'ordre de la photo par glisser-déposer" onclick="return false;"><i class="fa fa-fw fa-sort"></i></a></div>';
                    $retour->html .= '<div class="galerie-filelist-photo-supprimer"><a href="#" title="Supprimer la photo"><i class="fa fa-fw fa-trash-o"></i></a></div>';
                    $retour->html .= '</div>';
                }
            }
        }

        $this->application->viderLeCache();

        echo json_encode($retour);
    }

    public function modifierPhotoAction()
    {
        $this->estConnecte();

        header('Content-Type: application/json');
        $retour = new \stdClass();

        if (!empty($_POST['id_galerie_photo']) && is_numeric($_POST['id_galerie_photo'])) {
            $galerie_photo = $this->manager_galerie->getPhoto($_POST['id_galerie_photo']);
            if ($galerie_photo) {
                if (!empty($_POST['ordre']) && is_numeric($_POST['ordre'])) {
                    $galerie_photo->ordre = $_POST['ordre'];

                    $this->manager_galerie->updatePhoto($galerie_photo);
                } elseif (!empty($_POST['nom'])) {
                    $galerie_photo->legende = $_POST['nom'];

                    $this->smanager_galerie->updatePhoto($galerie_photo);
                }
            }
        }

        $this->application->viderLeCache();

        echo json_encode($retour);
    }

    public function supprimerPhotoAction()
    {
        $this->estConnecte();

        header('Content-Type: application/json');
        $retour = new \stdClass();

        if (!empty($_POST['id_galerie_photo']) && is_numeric($_POST['id_galerie_photo'])) {
            $galerie_photo = $this->manager_galerie->getPhoto($_POST['id_galerie_photo']);

            if ($galerie_photo) {
                unlink(__DIR__ . "/../../../web/downloads/gallery/thumb-" . $galerie_photo->fichier);
                unlink(__DIR__ . "/../../../web/downloads/gallery/" . $galerie_photo->fichier);

                $this->manager_galerie->deletePhoto($galerie_photo->id);
            }
        }

        $this->application->viderLeCache();

        echo json_encode($retour);
    }

    // ---------- Posts ---------- //
    public function postsAction()
    {
        $this->estConnecte();

        $this->total = $this->manager_post->total($this->application->language, $this->datas['type']);
        $this->page = (!empty($this->datas['page']) && $this->datas['page'] <= ceil($this->total / $this->limite_par_page)) ? $this->datas['page'] : 1;

        if (!empty($this->datas['sens_tri'])) {
            $this->sens_tri = $this->datas['sens_tri'];
        }
        if (!empty($this->datas['champ_tri'])) {
            $this->champ_tri = $this->datas['champ_tri'];
        } else {
            $this->champ_tri = "id";
        }

        $this->posts = $this->manager_post->getPage($this->application->language, $this->datas['type'], $this->page, $this->limite_par_page, "", $this->champ_tri, $this->sens_tri);

        $this->datas['menu_actif'] = 3;

        $this->template("manager-header.php");
        $this->template("posts.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function ajouterUnPostAction()
    {
        $this->estConnecte();

        if ($_POST) {
            if (!\core\Securite::verifToken($_POST['token'])) {
                trigger_error("Ajout d'une nouveau post via un token invalide !", E_USER_ERROR);
                exit();
            }

            $message_d_erreur = "";
            if (empty($_POST['titre'])) {
                $message_d_erreur .= "<br /> - Un titre est obligatoire";
            }
            if (empty($_POST['texte'])) {
                $message_d_erreur .= "<br /> - Un texte est obligatoire";
            }
            if (empty($_POST['date'])) {
                $message_d_erreur .= "<br /> - Une date est obligatoire";
            }

            if (empty($message_d_erreur)) {
                $actif = (!empty($_POST['actif'])) ? 1 : 0;

                $this->post = new \models\Post(array(
                    "titre" => $_POST['titre'],
                    "sous_titre" => $_POST['sous_titre'],
                    "date" => strtotime(substr($_POST['date'], 3, 2) . "/" . substr($_POST['date'], 0, 2) . "/" . substr($_POST['date'], 6, 4)),
                    "ordre" => $_POST['ordre'],
                    "texte" => $_POST['texte'],
                    "actif" => $actif,
                    "langue" => $this->application->language,
                    "type" => $this->datas['type']
                ));

                $this->manager_post->insert($this->post);

                $id_post = \core\Application::$bdd->lastInsertId();
                $this->post->id = $id_post;

                $this->manager_post->ordreRemise($this->post, $this->application->language, $this->datas['type']);

                $this->application->viderLeCache();

                // Log ajout d'un post
                $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Ajout d'un post de type : " . $this->datas['type'] . " -> " . htmlspecialchars($_POST['titre']))));

                $_SESSION['message_info'] = "Post ajouté avec succès.";

                $this->application->redirection("manager/posts-" . $this->datas['type'] . "/modifier-un-post/" . $id_post . "/retour-" . $this->datas['retour_url']);
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $message_d_erreur;
            }
        }

        $this->max = $this->manager_post->returnMaxOrdre($this->application->language, $this->datas['type']);

        $this->datas['menu_actif'] = 3;

        $this->css[] = "jquery-ui.min.css";
        $this->javascript[] = "jquery-ui.min.js";

        $this->template("manager-header.php");
        $this->template("ajouter-un-post.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function modifierUnPostAction()
    {
        $this->estConnecte();

        $this->post = $this->manager_post->get($this->datas['id_post']);
        if (!$this->post) {
            $_SESSION['message_d_erreur'] = "Le post demandé n'existe pas !";
            $this->application->redirection("manager/posts-" . $this->datas['type'] . "/" . $this->datas['retour_url']);
        }

        $this->post_images = $this->manager_post->getAllImage($this->post->id);
        $this->post_documents = $this->manager_post->getAllDocument($this->post->id);

        if (!empty($_POST['action']) && $_POST['action'] == "update") {
            if (!\core\Securite::verifToken($_POST['token'])) {
                trigger_error("Modification d'un post via un token invalide !", E_USER_ERROR);
                exit();
            }

            $message_d_erreur = "";
            if (empty($_POST['titre'])) {
                $message_d_erreur .= "<br /> - Un titre est obligatoire";
            }
            if (empty($_POST['texte'])) {
                $message_d_erreur .= "<br /> - Un texte est obligatoire";
            }
            if (empty($_POST['date'])) {
                $message_d_erreur .= "<br /> - Une date est obligatoire";
            }

            if (empty($message_d_erreur)) {
                $actif = (!empty($_POST['actif'])) ? 1 : 0;

                $this->post->hydrate(array(
                    "titre" => $_POST['titre'],
                    "sous_titre" => $_POST['sous_titre'],
                    "date" => strtotime(substr($_POST['date'], 3, 2) . "/" . substr($_POST['date'], 0, 2) . "/" . substr($_POST['date'], 6, 4)),
                    "ordre" => $_POST['ordre'],
                    "texte" => $_POST['texte'],
                    "actif" => $actif
                ));

                $this->manager_post->update($this->post);

                $this->manager_post->ordreRemise($this->post, $this->application->language, $this->datas['type']);

                $this->application->viderLeCache();

                // Log modification d'un post
                $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Modification d'un post de type :" . $this->datas['type'] . " -> " . htmlspecialchars($_POST['titre']))));

                $_SESSION['message_info'] = "Post modifié avec succès.";

                $this->application->redirection("manager/posts-" . $this->datas['type'] . "/" . $this->datas['retour_url']);
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $message_d_erreur;
            }
        }

        if (!empty($_POST['action']) && $_POST['action'] == "add_image") {
            $message_d_erreur = "";
            if (empty($_FILES['fichier']['tmp_name'])) {
                $message_d_erreur .= "<br />- Merci de renseigner une image";
            }
            if (!empty($_FILES['fichier']['tmp_name'])) {
                $extensions = array('.jpg', '.JPG', '.jpeg', '.JPEG');
                $extension = strrchr($_FILES['fichier']['name'], '.');
                if (!in_array($extension, $extensions) || $_FILES['fichier']['size'] > 4194304) {
                    $message_d_erreur .= "<br />- Format ou poids de l'image incorrect !";
                }
            }

            if (empty($message_d_erreur)) {
                $fichier = slugifier\slugify(str_replace(array('.jpg', '.JPG', '.jpeg', '.JPEG'), array("", "", "", ""), $_FILES['fichier']['name'])) . "-" . rand(1, 100) . ".jpg";
                move_uploaded_file($_FILES['fichier']['tmp_name'], __DIR__ . "/../../../web/downloads/posts/" . $fichier);
                $fichier_resize = new \Eventviva\ImageResize(__DIR__ . "/../../../web/downloads/posts/" . $fichier);
                $fichier_resize->crop(110, 110)->save(__DIR__ . "/../../../web/downloads/posts/thumb-" . $fichier);

                $post_image = new \models\Post_image(array(
                    "id_post" => $this->post->id,
                    "fichier" => $fichier,
                    "legende" => $_POST['legende'],
                    "ordre" => $this->manager_post->returnMaxOrdreImage($this->post->id)
                ));

                $this->manager_post->insertImage($post_image);

                $this->application->viderLeCache();

                $_SESSION['message_info'] = "Image ajoutée";

                $this->application->redirection("manager/posts-" . $this->datas['type'] . "/modifier-un-post/" . $this->post->id . "/retour-" . $this->datas['retour_url']);
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $message_d_erreur;
            }
        }

        if (!empty($_POST['action']) && $_POST['action'] == "add_document") {
            $message_d_erreur = "";
            if (empty($_FILES['fichier']['tmp_name'])) {
                $message_d_erreur .= "<br />- Merci de renseigner un PDF";
            }
            if (!empty($_FILES['fichier']['tmp_name'])) {
                $extensions = array('.pdf', '.PDF');
                $extension = strrchr($_FILES['fichier']['name'], '.');
                if (!in_array($extension, $extensions) || $_FILES['fichier']['size'] > 4194304) {
                    $message_d_erreur .= "<br />- Format ou poids du PDF incorrect !";
                }
            }

            if (empty($message_d_erreur)) {
                $fichier = slugifier\slugify(str_replace(array('.pdf', '.PDF'), array("", ""), $_FILES['fichier']['name'])) . "-" . rand(1, 100) . ".pdf";
                move_uploaded_file($_FILES['fichier']['tmp_name'], __DIR__ . "/../../../web/downloads/posts/" . $fichier);

                $post_document = new \models\Post_document(array(
                    "id_post" => $this->post->id,
                    "fichier" => $fichier,
                    "legende" => $_POST['legende'],
                    "ordre" => $this->manager_post->returnMaxOrdreDocument($this->post->id)
                ));

                $this->manager_post->insertDocument($post_document);

                $this->application->viderLeCache();

                $_SESSION['message_info'] = "Document ajouté";

                $this->application->redirection("manager/posts-" . $this->datas['type'] . "/modifier-un-post/" . $this->post->id . "/retour-" . $this->datas['retour_url']);
            } else {
                $_SESSION['message_d_erreur'] = "Merci de vérifier les erreurs suivantes : <br />" . $message_d_erreur;
            }
        }

        $this->datas['menu_actif'] = 3;

        $this->css[] = "jquery-ui.min.css";
        $this->javascript[] = "jquery-ui.min.js";

        $this->template("manager-header.php");
        $this->template("modifier-un-post.php", "Manager");
        $this->template("manager-footer.php");
    }

    public function supprimerUnPostAction()
    {
        $this->estConnecte();

        $this->post = $this->manager_post->get($this->datas['id_post']);
        if (!$this->post) {
            $_SESSION['message_d_erreur'] = "Le post demandé n'existe pas !";
            $this->application->redirection("manager/posts-" . $this->datas['type'] . "/" . $this->datas['retour_url']);
        }

        $this->manager_post->delete($this->post->id);

        $images = $this->manager_post->getAllImage($this->post->id);
        foreach ($images as $image) {
            unlink(__DIR__ . "/../../../web/downloads/posts/" . $image->fichier);
            unlink(__DIR__ . "/../../../web/downloads/posts/thumb-" . $image->fichier);
        }
        $this->manager_post->deleteAllImage($this->post->id);

        $documents = $this->manager_post->getAllDocument($this->post->id);
        foreach ($documents as $document) {
            unlink(__DIR__ . "/../../../web/downloads/posts/" . $document->fichier);
        }
        $this->manager_post->deleteAllDocument($this->post->id);

        $this->application->viderLeCache();

        // Log suppression du post
        $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Suppression du post type : " . $this->datas['type'] . " -> " . htmlspecialchars($this->post->titre))));

        $_SESSION['message_info'] = "Post supprimé";

        $this->application->redirection("manager/posts-" . $this->datas['type'] . "/" . $this->datas['retour_url']);
    }

    public function supprimerUnPostImageAction()
    {
        $this->estConnecte();

        $image = $this->manager_post->getImage($this->datas['id_post_image']);
        if (!$image) {
            $this->application->redirection("manager/posts-" . $this->datas['type'] . "/modifier-un-post/" . $this->datas['id_post'] . "/retour-" . $this->datas['retour_url']);
        }

        $this->manager_post->deleteImage($image->id);

        unlink(__DIR__ . "/../../../web/downloads/posts/" . $image->fichier);
        unlink(__DIR__ . "/../../../web/downloads/posts/thumb-" . $image->fichier);

        $this->application->viderLeCache();

        $_SESSION['message_info'] = "Image supprimée";

        $this->application->redirection("manager/posts-" . $this->datas['type'] . "/modifier-un-post/" . $this->datas['id_post'] . "/retour-" . $this->datas['retour_url']);
    }

    public function ordrePostImageAction()
    {
        $this->estConnecte();

        header('Content-Type: application/json');
        $retour = new \stdClass();

        if (empty($_POST['id_post_image']) && empty($_POST['ordre'])) {
            $this->application->redirection("manager");
        }

        $image = $this->manager_post->getImage($_POST['id_post_image']);

        if ($image) {
            $image->ordre = $_POST['ordre'];
            $this->manager_post->updateImage($image);
        }

        $this->application->viderLeCache();

        echo json_encode($retour);
    }

    public function supprimerUnPostDocumentAction()
    {
        $this->estConnecte();

        $document = $this->manager_post->getDocument($this->datas['id_post_document']);
        if (!$document) {
            $this->application->redirection("manager/posts-" . $this->datas['type'] . "/modifier-un-post/" . $this->datas['id_post'] . "/retour-" . $this->datas['retour_url']);
        }

        $this->manager_post->deleteDocument($document->id);

        unlink(__DIR__ . "/../../../web/downloads/posts/" . $document->fichier);

        $this->application->viderLeCache();

        $_SESSION['message_info'] = "Document supprimé";

        $this->application->redirection("manager/posts-" . $this->datas['type'] . "/modifier-un-post/" . $this->datas['id_post'] . "/retour-" . $this->datas['retour_url']);
    }

    public function dupliquerPostAction()
    {
        $this->estConnecte();

        $this->post = $this->manager_post->get($this->datas['id_post']);
        if (!$this->post) {
            $_SESSION['message_d_erreur'] = "Le post a dupliqué n'existe pas !";
            $this->application->redirection("manager/posts-" . $this->datas['type'] . "/" . $this->datas['retour_url']);
        }

        $this->post->id = null;
        $this->post->date = time();
        $this->post->ordre = 1;
        $this->post->actif = 0;

        $this->manager_post->insert($this->post);

        $this->post->id = \core\Application::$bdd->lastInsertId();

        $images = $this->manager_post->getAllImage($this->datas['id_post']);
        foreach ($images as $image) {
            $fichier = time() . "-" . $image->fichier;
            copy(__DIR__ . "/../../../web/downloads/posts/" . $image->fichier, __DIR__ . "/../../../web/downloads/posts/" . $fichier);
            copy(__DIR__ . "/../../../web/downloads/posts/thumb-" . $image->fichier, __DIR__ . "/../../../web/downloads/posts/thumb-" . $fichier);
            $image->fichier = $fichier;
            $image->id_post = $this->post->id;
            $this->manager_post->insertImage($image);
        }

        $documents = $this->manager_post->getAllDocument($this->datas['id_post']);
        foreach ($documents as $document) {
            $fichier = time() . "-" . $document->fichier;
            copy(__DIR__ . "/../../../web/downloads/posts/" . $document->fichier, __DIR__ . "/../../../web/downloads/posts/" . $fichier);
            $document->fichier = $fichier;
            $document->id_post = $this->post->id;
            $this->manager_post->insertDocument($document);
        }

        $this->manager_post->ordreRemise($this->post, $this->application->language, $this->datas['type']);

        $this->application->viderLeCache();

        // Log dupliquation dun post
        $this->manager_log_manager->insert(new \models\Log_manager(array("date" => time(), "qui" => $this->manager, "action" => "Dupliquation d'un post type : " . $this->datas['type'] . " -> " . htmlspecialchars($this->post->titre))));

        $_SESSION['message_info'] = "Post dupliqué avec succès.";

        $this->application->redirection("manager/posts-" . $this->datas['type'] . "/modifier-un-post/" . $this->post->id . "/retour-" . $this->datas['retour_url']);
    }
}
