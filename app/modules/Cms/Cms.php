<?php

namespace app\modules\Cms;

class Cms extends \core\Module
{
    
    public function __construct(\core\Application $application, $datas = array())
    {
        parent::__construct($application, $datas);
        
        // We add the "template" folder of the "CMS" module to the list of directories containing templates twig
        $this->application->twig_loader->prependPath(__DIR__ . "/templates");
    }

    public function indexAction()
    {
        echo $this->application->twig->render('home.twig');
    }
}
