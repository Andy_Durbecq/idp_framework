<?php

namespace app\modules\Generator;

class Generator extends \core\Module
{

    private $table;
    private $struc_table = array();
    private $model = "";
    private $model_manager = "";
    private $primary_key;

    public function modelAction()
    {
        set_time_limit(0);

        // Name of the table
        $this->table = $this->datas['table'];

        // Table exist
        $req_table_exist = \core\Application::$bdd->prepare("SHOW TABLES LIKE :table;");
        $req_table_exist->execute(array(
            ":table" => $this->table
        ));
        $res_table_exist = $req_table_exist->fetchAll(\PDO::FETCH_ASSOC);

        if (!empty($res_table_exist)) {
            // Information of the table
            $req_struc_table = \core\Application::$bdd->query("DESCRIBE " . $this->table . ";");
            $res_struc_table = $req_struc_table->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($res_struc_table as $ligne) {
                if (!empty($ligne['Key']) && $ligne['Key'] == "PRI") {
                    $this->primary_key = $ligne;
                }
                $this->struc_table[] = $ligne;
            }

            // Model file
            $this->model = "<?php\n\nnamespace models;\n\nClass " . ucfirst($this->table) . " extends \core\Model {\n\n";

            foreach ($this->struc_table as $ligne) {
                $type = substr($ligne['Type'], 0, (strpos($ligne['Type'], "(")) ? strpos($ligne['Type'], "(") : strlen($ligne['Type']));
                $field = $ligne['Field'];
                $this->model .= "\t/**\n\t* @var " . $type . " $" . $field . "\n\t* @access public\n\t*/ \n\tpublic $" . $field . ";\n\n";
            }

            $this->model .= "\t/*\n\t* Formatting before display\n\t*\n\t* @return string\n\t* @access public\n\t*/\n\tpublic function __tostring () {\n\n\t}\n\n}";

            // Save file model
            if ($handle = fopen(__DIR__ . "/../../../models/" . ucfirst($this->table) . ".php", 'w')) {
                fwrite($handle, $this->model);
                fclose($handle);
            } else {
                echo "Erreur lors de l'enregistrement du fichier modèle !";
            }

            // Build manager File
            $insert_chaine = "";
            $value_chaine = "";
            $first = true;
            foreach ($this->struc_table as $ligne) {
                if (empty($ligne['Key'])) {
                    $field = $ligne['Field'];
                    if ($first) {
                        $insert_chaine = "`" . $field . "`";
                        $value_chaine = ":" . $field;
                    } else {
                        $insert_chaine .= ", `" . $field . "`";
                        $value_chaine .= ", :" . $field;
                    }
                    $first = false;
                }
            }

            $this->model_manager = "<?php\n\nnamespace models;\n\nuse \PDO;\n\nClass " . ucfirst($this->table) . "Manager {\n\n\t/**\n\t* Instance BDD\n\t*\n\t* @var object \$bdd\n\t* @access private\n\t*/\n\tprivate \$bdd;\n\n";
            $this->model_manager .= "\t/**\n\t* Constructeur de la classe\n\t*\n\t* @param object \$bdd Lien de la base de données\n\t* @access public\n\t* @return void\n\t*/\n\tpublic function __construct(\$bdd) {\n\t\t\$this->bdd = \$bdd;\n\t}\n\n";

            // Insert function
            $this->model_manager .= "\t/**\n\t* Insertion\n\t*\n\t* @param " . ucfirst($this->table) . " \$" . $this->table . "\n\t* @access public\n\t* @return void\n\t*/\n";
            $this->model_manager .= "\tpublic function insert(" . ucfirst($this->table) . " \$" . $this->table . ") {\n";
            $this->model_manager .= "\t\t\$requete = \$this->bdd->prepare(\"INSERT INTO `" . $this->table . "` (" . $insert_chaine . ") VALUES (" . $value_chaine . ");\");\n";
            $this->model_manager .= "\t\t\$requete->execute(array(\n";
            foreach ($this->struc_table as $ligne) {
                if (empty($ligne['Key'])) {
                    if ($ligne['Field'] == $this->struc_table[count($this->struc_table) - 1]['Field']) {
                        $this->model_manager .= "\t\t\t\":" . $ligne['Field'] . "\" => \$" . $this->table . "->" . $ligne['Field'] . "\n";
                    } else {
                        $this->model_manager .= "\t\t\t\":" . $ligne['Field'] . "\" => \$" . $this->table . "->" . $ligne['Field'] . ",\n";
                    }
                }
            }
            $this->model_manager .= "\t\t));\n";
            $this->model_manager .= "\t}\n\n";

            // Count function
            $this->model_manager .= "\t/**\n\t* Compte le nombre d'enregistrement\n\t*\n\t* @access public\n\t* @return int\n\t*/\n";
            $this->model_manager .= "\tpublic function count() {\n\t\t\$requete = \$this->bdd->query(\"SELECT * FROM `" . $this->table . "`;\");\n\t\treturn count(\$requete->fetchAll());\n\t}\n\n";

            // Delete function
            if (!empty($this->primary_key)) {
                $type_key = substr($this->primary_key['Type'], 0, (strpos($this->primary_key['Type'], "(")) ? strpos($this->primary_key['Type'], "(") : strlen($this->primary_key['Type']));
                $this->model_manager .= "\t/**\n\t* Suppression\n\t*\n\t* @param " . $type_key . " \$" . $this->primary_key['Field'] . " Identifiant unique\n\t* @access public\n\t* @return void\n\t*/\n";
                $this->model_manager .= "\tpublic function delete(\$" . $this->primary_key['Field'] . ") {\n";
                $this->model_manager .= "\t\t\$requete = \$this->bdd->prepare(\"DELETE FROM `" . $this->table . "` WHERE `" . $this->primary_key['Field'] . "` = :" . $this->primary_key['Field'] . ";\");\n";
                $this->model_manager .= "\t\t\$requete->execute(array(\n\t\t\t\":" . $this->primary_key['Field'] . "\" => \$" . $this->primary_key['Field'] . "\n\t\t));\n";
                $this->model_manager .= "\t}\n\n";
            }

            // Update function
            if (!empty($this->primary_key)) {
                $update_chaine = "";
                $first = true;
                foreach ($this->struc_table as $ligne) {
                    if (empty($ligne['Key'])) {
                        $field = $ligne['Field'];
                        if ($first) {
                            $update_chaine = "`" . $field . "` = :" . $field;
                        } else {
                            $update_chaine .= ", `" . $field . "` = :" . $field;
                        }
                        $first = false;
                    }
                }
                $this->model_manager .= "\t/**\n\t* Modification\n\t*\n\t* @param " . ucfirst($this->table) . " \$" . $this->table . "\n\t* @access public\n\t* @return void\n\t*/\n";
                $this->model_manager .= "\tpublic function update(" . ucfirst($this->table) . " \$" . $this->table . ") {\n";
                $this->model_manager .= "\t\t\$requete = \$this->bdd->prepare(\"UPDATE `" . $this->table . "` SET " . $update_chaine . " WHERE `" . $this->primary_key['Field'] . "` = :" . $this->primary_key['Field'] . ";\");\n";
                $this->model_manager .= "\t\t\$requete->execute(array(\n";
                foreach ($this->struc_table as $ligne) {
                    if ($ligne['Field'] == $this->struc_table[count($this->struc_table) - 1]['Field']) {
                        $this->model_manager .= "\t\t\t\":" . $ligne['Field'] . "\" => \$" . $this->table . "->" . $ligne['Field'] . "\n";
                    } else {
                        $this->model_manager .= "\t\t\t\":" . $ligne['Field'] . "\" => \$" . $this->table . "->" . $ligne['Field'] . ",\n";
                    }
                }
                $this->model_manager .= "\t\t));\n";
                $this->model_manager .= "\t}\n\n";
            }

            // Get function
            if (!empty($this->primary_key)) {
                $type_key = substr($this->primary_key['Type'], 0, (strpos($this->primary_key['Type'], "(")) ? strpos($this->primary_key['Type'], "(") : strlen($this->primary_key['Type']));
                $this->model_manager .= "\t/**\n\t* Retourne une entrée\n\t*\n\t* @param " . $type_key . " \$" . $this->primary_key['Field'] . " Identifiant unique\n\t* @access public\n\t* @return " . ucfirst($this->table) . "\n\t*/\n";
                $this->model_manager .= "\tpublic function get(\$" . $this->primary_key['Field'] . ") {\n";
                $this->model_manager .= "\t\t\$requete = \$this->bdd->prepare(\"SELECT * FROM `" . $this->table . "` WHERE `" . $this->primary_key['Field'] . "` = :" . $this->primary_key['Field'] . ";\");\n";
                $this->model_manager .= "\t\t\$requete->execute(array(\n\t\t\t\":" . $this->primary_key['Field'] . "\" => \$" . $this->primary_key['Field'] . "\n\t\t));\n";
                $this->model_manager .= "\t\t\$donnees = \$requete->fetchAll(PDO::FETCH_ASSOC);\n";
                $this->model_manager .= "\t\tif (count(\$donnees) == 1) {\n";
                $this->model_manager .= "\t\t\treturn new " . ucfirst($this->table) . "(\$donnees[0]);\n\t\t}\n";
                $this->model_manager .= "\t\telse {\n\t\t\treturn false;\n\t\t}\n";
                $this->model_manager .= "\t}\n\n";
            }

            // GetAll function
            $this->model_manager .= "\t/**\n\t* Retourne toutes les entrées\n\t*\n\t* @access public\n\t* @return Array\n\t*/\n";
            $this->model_manager .= "\tpublic function getAll() {\n";
            $this->model_manager .= "\t\t\$retour = array();\n";
            $this->model_manager .= "\t\t\$requete = \$this->bdd->query(\"SELECT * FROM `" . $this->table . "`;\");\n";
            $this->model_manager .= "\t\t\$resultat = \$requete->fetchAll(PDO::FETCH_ASSOC);\n";
            $this->model_manager .= "\t\tforeach (\$resultat as \$donnees) {\n";
            $this->model_manager .= "\t\t\t\$retour[] = new " . ucfirst($this->table) . "(\$donnees);\n";
            $this->model_manager .= "\t\t}\n";
            $this->model_manager .= "\t\treturn \$retour;\n";
            $this->model_manager .= "\t}\n\n";

            // End
            $this->model_manager .= "}";

            // Save file manager
            if ($handle = fopen(__DIR__ . "/../../../models/" . ucfirst($this->table) . "Manager.php", 'w')) {
                fwrite($handle, $this->model_manager);
                fclose($handle);
            } else {
                echo "Erreur lors de l'enregistrement du fichier manager !";
            }
        } else {
            echo "La table demandée n'existe pas en BDD !";
        }
    }
}
