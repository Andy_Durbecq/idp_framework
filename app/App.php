<?php

namespace app;

class App extends \core\Application
{

    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $this->launch();        
    }
}
