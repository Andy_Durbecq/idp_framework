<?php

namespace core;

class Config
{

    /**
     * @var Application
     * @access protected
     */
    private $application;

    /**
     * @var string
     * @access public
     */
    public $url;

    /**
     * @var string
     * @access public
     */
    public $name;

    /**
     * @var string
     * @access public
     */
    public $mail_contact;

    /**
     * @var string
     * @access public
     */
    public $host_bdd;

    /**
     * @var string
     * @access public
     */
    public $user_bdd;

    /**
     * @var string
     * @access public
     */
    public $pass_bdd;

    /**
     * @var string
     * @access public
     */
    public $base_bdd;

    /**
     * @var string
     * @access public
     */
    public $date_timezone;

    /**
     * @var string
     * @access public
     */
    public $google_analytics;

    /**
     * @var array
     * @access public
     */
    public $languages = array();

    /**
     * @var string
     * @access public
     */
    public $hash_key;

    /**
     * @var boolean
     * @access public
     */
    public $cache;

    /**
     * SwiftMailer
     */
    public $swift_actif;
    public $swift_smtp;
    public $swift_port;
    public $swift_login;
    public $swift_pass;
    
    /**
     * @var string
     * @access public
     */
    public $recaptcha_public_key;
    
    /**
     * @var string
     * @access public
     */
    public $recaptcha_secret_key;

    /**
     * @access public
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
        $xml = new \DOMDocument;
        if (is_file(__DIR__ . '/../app/config/app.xml')) {
            $xml->load(__DIR__ . '/../app/config/app.xml');
        } else {
            trigger_error("Error loading app.xml!", E_USER_ERROR);
            exit();
        }

        $elements = $xml->getElementsByTagName('parameter');

        // Browsing all the parameters of the application
        foreach ($elements as $element) {
            $variable = $element->getAttribute('var');
            if (property_exists($this, $variable)) {
                if ($variable == "languages") {
                    foreach (explode(',', $element->getAttribute('value')) as $value) {
                        $this->languages[] = $value;
                    }
                } elseif ($variable == "url") {
                    $urls = explode(',', $element->getAttribute('value'));
                    if (!empty($urls) && in_array("http://" . $_SERVER['HTTP_HOST'], $urls)) {
                        $this->$variable = "http://" . $_SERVER['HTTP_HOST'];
                    } else {
                        trigger_error("The url requested is invalid or incorrect!", E_USER_ERROR);
                        exit();
                    }
                } else {
                    $this->$variable = $element->getAttribute('value');
                }
            }
        }
    }

    /**
     * @param type $variable
     * @access public
     * @return string
     */
    public function __get($variable)
    {
        if (property_exists($this, $variable)) {
            return $this->$variable;
        } else {
            return null;
        }
    }
}
