<?php

namespace core;

use \PDO;

abstract class Application
{

    /**
     * @var Config
     * @access public
     */
    public $config;

    /**
     *
     * @var array
     * @access protected
     */
    protected $translation;

    /**
     *
     * @var string
     * @access public
     */
    public $language;

    /**
     *
     * @var PDO
     * @static
     */
    public static $bdd;

    /**
     * All variables in Url
     *
     * @var array
     * @access protected
     */
    private $variables_url = array();

    /**
     * All variables in Url with mask
     *
     * @var array
     * @access protected
     */
    private $variables_get = array();

    /**
     * Module in progress
     *
     * @var string
     * @access public
     */
    public $module;

    /**
     * Ongoing action
     *
     * @var string
     * @access public
     */
    public $action;

    /**
     * Swiftmailer
     */
    public $swiftmailer;

    /**
     * Twig
     */
    public $twig;

    /**
     * Loader Twig
     */
    public $twig_loader;

    /**
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->config = new Config($this);

        $this->dateTimeZone();

        $this->instanceBdd();

        $this->checkUrl();

        // SwiftMailer
        if ($this->config->swift_actif) {
            $transport = \Swift_SmtpTransport::newInstance($this->config->swift_smtp, $this->config->swift_port)
                    ->setAuthMode("login")
                    ->setUsername($this->config->swift_login)
                    ->setPassword($this->config->swift_pass);
            $this->swiftmailer = \Swift_Mailer::newInstance($transport);
            //$this->swiftmailer->getTransport()->start();
        }

        // Twig
        $this->twig_loader = new \Twig_Loader_Filesystem(__DIR__ . "/../app/templates");
        $this->twig = new \Twig_Environment($this->twig_loader, array(
            'cache' => ($this->config->cache) ? __DIR__ . "/../cache" : false
        ));
        $this->twig->addGlobal("WEB_DIR_URL", $this->config->url);
        $this->twig->addGlobal("WEB_URL", $this->getUrl());
        $this->twig->addGlobal("TITLE", $this->config->name);
        $this->twig->addGlobal("MAIL_CONTACT", $this->config->mail_contact);
        $this->twig->addGlobal("GOOGLE_ANALYTICS", $this->config->google_analytics);
        $this->twig->addGlobal("CURRENT_LANGUAGE", $this->language);
        $this->twig->addGlobal("RECAPTCHA_PUBLIC_KEY", $this->config->recaptcha_public_key);
        $this->twig->addGlobal("RECAPTCHA_SECRET_KEY", $this->config->recaptcha_secret_key);
        $this->twig->addGlobal("TRANSLATION", $this->translation);
        // Adding custom extension class
        $this->twig->addExtension(new \core\TwigExtension());
    }

    /**
     * @access private
     * @return void
     */
    private function dateTimeZone()
    {
        if ($this->config->date_timezone) {
            date_default_timezone_set($this->config->date_timezone);
        } else {
            date_default_timezone_set('Europe/Paris');
        }
    }

    /**
     * @access private
     * @return void
     */
    private function instanceBdd()
    {
        if (!empty($this->config->base_bdd)) {
            if (!isset(self::$bdd)) {
                try {
                    self::$bdd = new \PDO("mysql:host=" . $this->config->host_bdd . ";dbname=" . $this->config->base_bdd . "", $this->config->user_bdd, $this->config->pass_bdd);
                    self::$bdd->exec("SET CHARACTER SET utf8");
                    self::$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                } catch (Exception $e) {
                    trigger_error("Error connecting to the database!", E_USER_ERROR);
                    exit();
                }
            }
        }
    }

    /**
     * Stores all the variables of the URL and defines the language before removing it from the variables
     *
     * @access private
     * @return void
     */
    private function checkUrl()
    {
        if (!empty($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != "/") {
            $url_finale = $_SERVER['REQUEST_URI'];
            $position_get = strpos($url_finale, "?");
            if ($position_get) {
                $url_finale = substr($url_finale, 0, $position_get);
            }
            $this->variables_url = explode("/", substr($url_finale, 1));
        }

        // Deleting the language of variables for processing
        if (!empty($this->variables_url) && in_array($this->variables_url[0], $this->config->languages)) {
            $this->language = $this->variables_url[0];
            unset($this->variables_url[0]);
            $this->variables_url = array_values($this->variables_url);
        } else {
            if (!empty($this->config->languages)) {
                $this->language = $this->config->languages[0];
            } else {
                trigger_error("Please set a default language in the application configuration file!", E_USER_ERROR);
                exit();
            }
        }

        if (is_file(__DIR__ . "/../translations/" . $this->language . ".php")) {
            require_once(__DIR__ . "/../translations/" . $this->language . ".php");
        }
    }

    /**
     * @access public
     * @return void
     */
    abstract public function run();

    /**
     * @param string $variable
     * @access public
     * @return string
     */
    public function __get($variable)
    {
        if (property_exists($this, $variable)) {
            return $this->$variable;
        } else {
            return null;
        }
    }

    /**
     * Translate
     *
     * @param string $texte_a_traduire
     * @return string
     */
    public function __($cle_texte_a_traduire)
    {
        if (!empty($this->translation[$cle_texte_a_traduire])) {
            return $this->translation[$cle_texte_a_traduire];
        } else {
            return $cle_texte_a_traduire;
        }
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getUrl()
    {
        $url = "";
        if (count($this->config->languages) > 1) {
            $url .= $this->config->url . "/" . $this->language;
        } else {
            $url .= $this->config->url;
        }
        return $url;
    }

    /**
     * @param string $module
     * @access public
     * @return null
     */
    public function redirection($url = "")
    {
        if (!empty($url)) {
            header("Location: " . $this->getUrl() . "/" . $url);
        } else {
            header("Location: " . $this->getUrl());
        }
        exit();
    }

    /**
     * @access protected
     * @return Module
     */
    protected function launch()
    {
        $xml = new \DOMDocument;
        if (is_file(__DIR__ . '/../app/config/routes.xml')) {
            $xml->load(__DIR__ . '/../app/config/routes.xml');
        } else {
            trigger_error("Error loading routes.xml!", E_USER_ERROR);
            exit();
        }

        $routes = $xml->getElementsByTagName('route');

        foreach ($routes as $route) {
            if ($route->hasAttribute('url') && $route->hasAttribute('module') && $route->hasAttribute('action')) {
                $pattern = $route->getAttribute('url');
                $this->module = $route->getAttribute('module');
                $this->action = $route->getAttribute('action') . "Action";
                if (preg_match("#^" . $pattern . "$#", "/" . implode("/", $this->variables_url), $this->variables_get)) {
                    $cont = "\\app\\modules\\" . $this->module . "\\" . $this->module;

                    // Deleting the entire reference
                    if (!empty($this->variables_get)) {
                        unset($this->variables_get[0]);
                        $this->variables_get = array_values($this->variables_get);
                    }

                    // Data for the controller
                    $datas = array();
                    if ($route->hasAttribute('vars')) {
                        $nom_datas = explode(",", $route->getAttribute("vars"));
                        $cpt = 0;
                        foreach ($nom_datas as $nom) {
                            if (!empty($this->variables_get[$cpt])) {
                                $datas[$nom] = $this->variables_get[$cpt];
                            }
                            $cpt++;
                        }
                    }

                    $controleur = new $cont($this, $datas);
                    if (method_exists($controleur, $this->action)) {
                        $action = $this->action;
                        $controleur->$action();

                        // Error/Info message
                        if (!empty($_SESSION['message_d_erreur'])) {
                            Application::messageFlash($_SESSION['message_d_erreur'], "erreur");
                            unset($_SESSION['message_d_erreur']);
                        } elseif (!empty($_SESSION['message_info'])) {
                            Application::messageFlash($_SESSION['message_info'], "info");
                            unset($_SESSION['message_info']);
                        }
                    } else {
                        trigger_error("Error loading action " . $this->action . " !", E_USER_ERROR);
                        exit();
                    }
                    return null;
                    break;
                }
            }
        }

        $this->redirection("404");
    }

    public function __clone()
    {
        return false;
    }

    public function __wakeup()
    {
        return false;
    }

    public static function messageFlash($message, $class)
    {
        echo "<script type='text/javascript'>";
        echo "message(\"" . $message . "\", \"" . $class . "\")";
        echo "</script>";
    }

    public function viderLeCache()
    {
        $this->DeleteDir(__DIR__ . "/../cache");
    }

    private function DeleteDir($path, $first = true)
    {
        if ((is_dir($path) === true)) {
            $files = array_diff(scandir($path), array('.', '..'));

            foreach ($files as $file) {
                $this->DeleteDir(realpath($path) . '/' . $file, false);
            }
            if (!$first) {
                return rmdir($path);
            }
        } elseif (is_file($path) === true) {
            if (basename($path) != "index.html") {
                return unlink($path);
            }
        }

        return false;
    }
}
