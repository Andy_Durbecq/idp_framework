<?php

namespace core;

abstract class Module
{

    /**
     *
     * @var Application
     * @access protected
     */
    protected $application;

    /**
     * @var array
     * @access public
     */
    public $css = array();

    /**
     * @var array
     * @access public
     */
    public $javascript = array();

    /**
     * @var array
     * @access public
     */
    public $datas = array();

    /**
     * @param Application $application
     * @access public
     * @return void
     */
    public function __construct(Application $application, $datas = array())
    {
        $this->application = $application;
        $this->datas = $datas;
    }

    /**
     * @param type $variable
     * @access public
     * @return string
     */
    public function __get($variable)
    {
        if (property_exists($this, $variable)) {
            return $this->$variable;
        } else {
            return "";
        }
    }
    
    /**
     * @param string $template 
     * @param string $module 
     * @access public
     * @retrun void
     */
    public function template($template, $module = "")
    {
        if (!empty($module) && is_string($module) && is_file(__DIR__ . "/../app/modules/" . $module . "/templates/" . $template)) {
            require_once (__DIR__ . "/../app/modules/" . $module . "/templates/" . $template);
        } elseif (is_file(__DIR__ . "/../app/templates/" . $template)) {
            require_once (__DIR__ . "/../app/templates/" . $template);
        } else {
            trigger_error("Template not exist!", E_USER_ERROR);
            exit();
        }
    }
}
