<?php

namespace models;

use \PDO;

class PostManager
{

    /**
     * Instance de la connexion à la BDD
     *
     * @var object $bdd
     * @access private
     */
    private $bdd;

    /**
     * Constructeur de la classe
     *
     * @param object $bdd Lien de la base de données
     * @access public
     * @return void
     */
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    /**
     * Insertion
     *
     * @param Post $post
     * @access public
     * @return void
     */
    public function insert(Post $post)
    {
        $requete = $this->bdd->prepare("INSERT INTO `post` (`titre`, `sous_titre`, `date`, `ordre`, `actif`, `texte`, `langue`, `type`) VALUES (:titre, :sous_titre, :date, :ordre, :actif, :texte, :langue, :type);");
        $requete->execute(array(
            ":titre" => $post->titre,
            ":sous_titre" => $post->sous_titre,
            ":date" => $post->date,
            ":ordre" => $post->ordre,
            ":actif" => $post->actif,
            ":texte" => $post->texte,
            ":langue" => $post->langue,
            ":type" => $post->type
        ));
    }

    /**
     * Suppression
     *
     * @param int $id Identifiant unique
     * @access public
     * @return void
     */
    public function delete($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `post` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }

    /**
     * Modification mais pas de la langue ni du type
     *
     * @param Post $post
     * @access public
     * @return void
     */
    public function update(Post $post)
    {
        $requete = $this->bdd->prepare("UPDATE `post` SET `titre` = :titre, `sous_titre` = :sous_titre, `date` = :date, `ordre` = :ordre, `actif` = :actif, `texte` = :texte WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $post->id,
            ":titre" => $post->titre,
            ":sous_titre" => $post->sous_titre,
            ":date" => $post->date,
            ":ordre" => $post->ordre,
            ":actif" => $post->actif,
            ":texte" => $post->texte
        ));
    }

    /**
     * Retourne une entrée
     *
     * @param int $id Identifiant unique
     * @access public
     * @return Post
     */
    public function get($id)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `post` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Post($donnees[0]);
        } else {
            return false;
        }
    }

    /**
     * Retourne toutes les entrées
     *
     * @access public
     * @return Array
     */
    public function getAll($langue, $type)
    {
        $retour = array();
        $requete = $this->bdd->prepare("SELECT * FROM `post` WHERE `actif` = 1 AND `langue` = :langue AND `type` = :type ORDER BY `ordre` ASC;");
        $requete->execute(array(
            ":langue" => $langue,
            ":type" => $type
        ));
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Post($donnees);
        }
        return $retour;
    }

    public function returnMaxOrdre($langue, $type)
    {
        $requete = $this->bdd->prepare("SELECT MAX(`ordre`) AS ordre FROM `post` WHERE `langue` = :langue AND `type` = :type;");
        $requete->execute(array(
            ":langue" => $langue,
            ":type" => $type
        ));
        $resulat = $requete->fetch(PDO::FETCH_ASSOC);
        if (empty($resulat['ordre'])) {
            $resulat['ordre'] = 0;
        }
        return $resulat['ordre'] + 1;
    }

    public function getPage($langue, $type, $page = 1, $limite = 10, $where = "", $ordre = "id", $sens_ordre = "ASC")
    {
        $index_debut = ($page - 1) * $limite;
        $retour = array();
        $requete = $this->bdd->query("SELECT * FROM `post` WHERE `langue` = '" . $langue . "' AND `type` = '" . $type . "'" . $where . " ORDER BY `" . $ordre . "` " . $sens_ordre . " LIMIT " . $index_debut . ", " . $limite . ";");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Post($donnees);
        }
        return $retour;
    }

    public function total($langue, $type)
    {
        $requete = $this->bdd->prepare("SELECT COUNT(*) AS total FROM `post` WHERE `langue` = :langue AND `type` = :type;");
        $requete->execute(array(
            ":langue" => $langue,
            ":type" => $type
        ));
        $resultat = $requete->fetch(PDO::FETCH_ASSOC);

        return $resultat['total'];
    }

    public function ordreRemise($post, $langue, $type)
    {
        $requete = $this->bdd->query("SELECT * FROM `post` WHERE `ordre` >= " . $post->ordre . " AND `id` != " . $post->id . " AND `langue` = '" . $langue . "' AND `type` = '" . $type . "' ORDER BY `ordre` ASC;");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        $ordre = $post->ordre + 1;
        foreach ($resultat as $donnees) {
            $post = new Post($donnees);
            $post->ordre = $ordre;
            $this->update($post);
            $ordre++;
        }
    }

    /* Post Images */

    public function returnMaxOrdreImage($id)
    {
        $requete = $this->bdd->query("SELECT MAX(`ordre`) AS ordre FROM `post_image` WHERE `id_post` = " . $id . ";");
        $resulat = $requete->fetch(PDO::FETCH_ASSOC);
        if (empty($resulat['ordre'])) {
            $resulat['ordre'] = 0;
        }
        return $resulat['ordre'] + 1;
    }

    public function insertImage(Post_image $post_image)
    {
        $requete = $this->bdd->prepare("INSERT INTO `post_image` (`id_post`, `fichier`, `legende`, `ordre`) VALUES (:id_post, :fichier, :legende, :ordre);");
        $requete->execute(array(
            ":id_post" => $post_image->id_post,
            ":fichier" => $post_image->fichier,
            ":legende" => $post_image->legende,
            ":ordre" => $post_image->ordre
        ));
    }

    public function updateImage(Post_image $post_image)
    {
        $requete = $this->bdd->prepare("UPDATE `post_image` SET `ordre` = :ordre WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $post_image->id,
            ":ordre" => $post_image->ordre
        ));
    }

    public function getImage($id)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `post_image` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Post_image($donnees[0]);
        } else {
            return false;
        }
    }

    public function getAllImage($id)
    {
        $retour = array();
        $requete = $this->bdd->prepare("SELECT * FROM `post_image` WHERE `id_post` = :id ORDER BY `ordre` ASC;");
        $requete->execute(array(
            ":id" => $id
        ));
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Post_image($donnees);
        }
        return $retour;
    }

    public function deleteImage($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `post_image` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }

    public function deleteAllImage($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `post_image` WHERE `id_post` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }

    /* Post Documents */

    public function returnMaxOrdreDocument($id)
    {
        $requete = $this->bdd->query("SELECT MAX(`ordre`) AS ordre FROM `post_document` WHERE `id_post` = " . $id . ";");
        $resulat = $requete->fetch(PDO::FETCH_ASSOC);
        if (empty($resulat['ordre'])) {
            $resulat['ordre'] = 0;
        }
        return $resulat['ordre'] + 1;
    }

    public function insertDocument(Post_document $post_document)
    {
        $requete = $this->bdd->prepare("INSERT INTO `post_document` (`id_post`, `fichier`, `legende`, `ordre`) VALUES (:id_post, :fichier, :legende, :ordre);");
        $requete->execute(array(
            ":id_post" => $post_document->id_post,
            ":fichier" => $post_document->fichier,
            ":legende" => $post_document->legende,
            ":ordre" => $post_document->ordre
        ));
    }

    public function updateDocument(Post_document $post_document)
    {
        $requete = $this->bdd->prepare("UPDATE `post_document` SET `ordre` = :ordre WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $post_document->id,
            ":ordre" => $post_document->ordre
        ));
    }

    public function getDocument($id)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `post_document` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Post_document($donnees[0]);
        } else {
            return false;
        }
    }

    public function getAllDocument($id)
    {
        $retour = array();
        $requete = $this->bdd->prepare("SELECT * FROM `post_document` WHERE `id_post` = :id ORDER BY `ordre` ASC;");
        $requete->execute(array(
            ":id" => $id
        ));
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Post_document($donnees);
        }
        return $retour;
    }

    public function deleteDocument($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `post_document` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }

    public function deleteAllDocument($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `post_document` WHERE `id_post` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }
}
