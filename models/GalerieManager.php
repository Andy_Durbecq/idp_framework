<?php

namespace models;

use \PDO;

class GalerieManager
{

    /**
     * Instance de la connexion à la BDD
     *
     * @var object $bdd
     * @access private
     */
    private $bdd;

    /**
     * Constructeur de la classe
     *
     * @param object $bdd Lien de la base de données
     * @access public
     * @return void
     */
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    /**
     * Insertion
     *
     * @param Galerie $galerie
     * @access public
     * @return void
     */
    public function insert(Galerie $galerie)
    {
        $requete = $this->bdd->prepare("INSERT INTO `galerie` (`nom`, `ordre`) VALUES (:nom, :ordre);");
        $requete->execute(array(
            ":nom" => $galerie->nom,
            ":ordre" => $galerie->ordre
        ));
    }

    /**
     * Suppression
     *
     * @param int $id Identifiant unique
     * @access public
     * @return void
     */
    public function delete($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `galerie` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }

    /**
     * Modification
     *
     * @param Galerie $galerie
     * @access public
     * @return void
     */
    public function update(Galerie $galerie)
    {
        $requete = $this->bdd->prepare("UPDATE `galerie` SET `nom` = :nom, `ordre` = :ordre WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $galerie->id,
            ":nom" => $galerie->nom,
            ":ordre" => $galerie->ordre
        ));
    }

    /**
     * Retourne une entrée
     *
     * @param int $id Identifiant unique
     * @access public
     * @return Galerie
     */
    public function get($id)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `galerie` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Galerie($donnees[0]);
        } else {
            return false;
        }
    }

    /**
     * Retourne toutes les entrées
     *
     * @access public
     * @return Array
     */
    public function getAll()
    {
        $retour = array();
        $requete = $this->bdd->query("SELECT * FROM `galerie` ORDER BY `ordre` ASC;");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Galerie($donnees);
        }
        return $retour;
    }

    public function remiseOrdre()
    {
        $galeries = $this->getAll();

        $ordre = 2;
        foreach ($galeries as $galerie) {
            $galerie->ordre = $ordre;
            $this->update($galerie);
            $ordre++;
        }
    }

    /* Visuels */

    /**
     * Insertion
     *
     * @param Galerie_photo $galerie_photo
     * @access public
     * @return void
     */
    public function insertPhoto(Galerie_photo $galerie_photo)
    {
        $requete = $this->bdd->prepare("INSERT INTO `galerie_photo` (`id_galerie`, `legende`, `fichier`, `ordre`) VALUES (:id_galerie, :legende, :fichier, :ordre);");
        $requete->execute(array(
            ":id_galerie" => $galerie_photo->id_galerie,
            ":legende" => $galerie_photo->legende,
            ":fichier" => $galerie_photo->fichier,
            ":ordre" => $galerie_photo->ordre
        ));
    }

    /**
     * Suppression
     *
     * @param int $id Identifiant unique
     * @access public
     * @return void
     */
    public function deletePhoto($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `galerie_photo` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }

    /**
     * Modification
     *
     * @param Galerie_photo $galerie_photo
     * @access public
     * @return void
     */
    public function updatePhoto(Galerie_photo $galerie_photo)
    {
        $requete = $this->bdd->prepare("UPDATE `galerie_photo` SET `id_galerie` = :id_galerie, `legende` = :legende, `fichier` = :fichier, `ordre` = :ordre WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $galerie_photo->id,
            ":id_galerie" => $galerie_photo->id_galerie,
            ":legende" => $galerie_photo->legende,
            ":fichier" => $galerie_photo->fichier,
            ":ordre" => $galerie_photo->ordre
        ));
    }

    /**
     * Retourne une entrée
     *
     * @param int $id Identifiant unique
     * @access public
     * @return Galerie_photo
     */
    public function getPhoto($id)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `galerie_photo` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Galerie_photo($donnees[0]);
        } else {
            return false;
        }
    }

    /**
     * Retourne toutes les entrées
     *
     * @access public
     * @return Array
     */
    public function getAllPhoto($id_galerie)
    {
        $retour = array();
        $requete = $this->bdd->prepare("SELECT * FROM `galerie_photo` WHERE `id_galerie` = :id_galerie ORDER BY `ordre` ASC;");
        $requete->execute(array(
            ":id_galerie" => $id_galerie
        ));
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Galerie_photo($donnees);
        }
        return $retour;
    }

    public function remiseOrdrePhoto($id_galerie)
    {
        $photos = $this->getAllPhoto($id_galerie);

        $ordre = 2;
        foreach ($photos as $photo) {
            $photo->ordre = $ordre;
            $this->updatePhoto($photo);
            $ordre++;
        }
    }
}
