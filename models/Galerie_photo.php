<?php

namespace models;

class Galerie_photo extends \core\Model
{

    /**
     * @var int $id
     * @access public
     */
    public $id;

    /**
     * @var int $id_galerie
     * @access public
     */
    public $id_galerie;

    /**
     * @var varchar $legende
     * @access public
     */
    public $legende;

    /**
     * @var varchar $fichier
     * @access public
     */
    public $fichier;

    /**
     * @var int $ordre
     * @access public
     */
    public $ordre;

    /*
     * Formatage avant affichage
     *
     * @return string
     * @access public
     */

    public function __tostring()
    {
        return $this->legende;
    }
}
