<?php

namespace models;

use \PDO;

class Log_managerManager
{

    /**
     * Instance de la connexion à la BDD
     *
     * @var object $bdd
     * @access private
     */
    private $bdd;

    /**
     * Constructeur de la classe
     *
     * @param object $bdd Lien de la base de données
     * @access public
     * @return void
     */
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    /**
     * Insertion
     *
     * @param Log_manager $log_manager
     * @access public
     * @return void
     */
    public function insert(Log_manager $log_manager)
    {
        $requete = $this->bdd->prepare("INSERT INTO `log_manager` (`date`, `qui`, `action`) VALUES (:date, :qui, :action);");
        $requete->execute(array(
            ":date" => $log_manager->date,
            ":qui" => $log_manager->qui,
            ":action" => $log_manager->action
        ));
    }

    /**
     * Suppression
     *
     * @access public
     * @return void
     */
    public function delete()
    {
        // 6 mois
        $date_exp = time() - (60 * 60 * 24 * 180);
        $requete = $this->bdd->query("DELETE FROM `log_manager` WHERE `date` <= " . $date_exp . ";");
    }

    /**
     * Retourne toutes les entrées
     *
     * @access public
     * @return Array
     */
    public function getAll($limit = 100)
    {
        $retour = array();
        $requete = $this->bdd->query("SELECT * FROM `log_manager` ORDER BY `date` DESC LIMIT " . $limit . ";");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Log_manager($donnees);
        }
        return $retour;
    }
}
