<?php

namespace models;

use \PDO;

class ManagerManager
{

    /**
     * Instance de la connexion à la BDD
     *
     * @access private
     */
    private $bdd;

    /**
     * Constructeur de la classe
     *
     * @param object $bdd Lien de la base de données
     * @access public
     * @return void
     */
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    /**
     * Insertion d'un nouvel utilisateur du manager
     *
     * @param Manager $manager Utilisateur à insérer en BDD
     * @access public
     * @return void
     */
    public function insert(Manager $manager)
    {
        $requete = $this->bdd->prepare("INSERT INTO `manager` (`nom`, `prenom`, `email`, `pass`, `statut`, `telephone`, `portable`, `description`)
                                         VALUES (:nom, :prenom, :email, :pass, :statut, :telephone, :portable, :description);");
        $requete->execute(array(
            ":nom" => $manager->nom,
            ":prenom" => $manager->prenom,
            ":email" => $manager->email,
            ":pass" => $manager->pass,
            ":statut" => $manager->statut,
            ":telephone" => $manager->telephone,
            ":portable" => $manager->portable,
            ":description" => $manager->description
        ));
    }

    /**
     * Compte le nombre d'utilisateur présent en BDD
     *
     * @access public
     * @return int
     */
    public function count()
    {
        $requete = $this->bdd->query("SELECT * FROM `manager`;");
        return count($requete->fetchAll());
    }

    /**
     * Suppression d'un utilisateur du manager
     *
     * @param int $id Identifiant unique de l'utilisateur
     * @access public
     * @return void
     */
    public function delete($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `manager` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }

    /**
     * Modification d'un utilisateur du manager
     *
     * @param Manager $manager Utilisateur à modifier
     * @access public
     * @return void
     */
    public function update(Manager $manager)
    {
        $requete = $this->bdd->prepare("UPDATE `manager` SET `nom` = :nom, `prenom` = :prenom, `email` = :email, `pass` = :pass, `statut` = :statut, `telephone` = :telephone, `portable` = :portable, `description` = :description WHERE `id` = :id;");
        $requete->execute(array(
            ":nom" => $manager->nom,
            ":prenom" => $manager->prenom,
            ":email" => $manager->email,
            ":pass" => $manager->pass,
            ":statut" => $manager->statut,
            ":telephone" => $manager->telephone,
            ":portable" => $manager->portable,
            ":description" => $manager->description,
            ":id" => $manager->id
        ));
    }

    /**
     * Retourne un utilisateur ou false si inexistant
     *
     * @param int $id Identifiant unique de l'utilisateur à retourner
     * @access public
     * @return Manager
     */
    public function get($id)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `manager` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Manager($donnees[0]);
        } else {
            return false;
        }
    }
    
    public function getEmail($email)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `manager` WHERE `email` = :email;");
        $requete->execute(array(
            ":email" => $email
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Manager($donnees[0]);
        } else {
            return false;
        }
    }

    /**
     * Retourne tous les utilisateurs
     *
     * y compris les utilisateurs non actif
     *
     * @access public
     * @return Array
     */
    public function getAll()
    {
        $retour = array();
        $requete = $this->bdd->query("SELECT * FROM `manager` ORDER BY `nom` DESC;");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Manager($donnees);
        }
        return $retour;
    }
    
    public function getPage($page = 1, $limite = 10, $where = "", $ordre = "id", $sens_ordre = "ASC")
    {
        $index_debut = ($page - 1) * $limite;
        $retour = array();
        $requete = $this->bdd->query("SELECT * FROM `manager` WHERE `id` > 0" . $where . " ORDER BY `" . $ordre . "` " . $sens_ordre . " LIMIT " . $index_debut . ", " . $limite . ";");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Manager($donnees);
        }
        return $retour;
    }

    /**
     * Retourne tous les utilisateurs actifs
     *
     * @access public
     * @return Array
     */
    public function getAllActif()
    {
        $retour = array();
        $requete = $this->bdd->query("SELECT * FROM `manager` WHERE `statut` = 1 ORDER BY `nom` DESC;");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Manager($donnees);
        }
        return $retour;
    }

    /**
     * Vérifie qu'un utilisateur existe via son identifiant unique
     *
     * @param int $id Identifiant unique de l'utilisateur
     * @access public
     * @return boolean
     */
    public function existe($id)
    {
        $requete = $this->bdd->prepare("SELECT `id` FROM `manager` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
        return (bool) count($requete->fetchAll());
    }

    /**
     * Permet de connecter un utilisateur via son login/mot de passe
     *
     * Retourne faux si utilisateur login ou mot de passe incorrect
     *
     * @param string $email Email de l'utilisateur
     * @param string $pass Mot de passe de l'utilisateur déjà crypté
     * @access public
     * @return Manager
     */
    public function connexion($email, $pass)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `manager` WHERE email = :email;");
        $requete->execute(array(":email" => $email));
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($resultat) && count($resultat) == 1) {
            if ($resultat[0]['pass'] == $pass) {
                $administrateur = new Manager($resultat[0]);
                if ($administrateur->id && $administrateur->statut) {
                    $_SESSION['session_manager'] = $resultat[0]['id'];
                    $_SESSION['session_ipaddr'] = $_SERVER['REMOTE_ADDR'];
                    $_SESSION['session_last_access'] = time();
                    return $administrateur;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Deconnexion de l'utilisateur et suppression des variables de session
     *
     * @access public
     * @return void
     */
    public function deconnexion()
    {
        unset($_SESSION['session_manager']);
        unset($_SESSION['session_ipaddr']);
        unset($_SESSION['session_last_access']);
    }

    /**
     * Permet de vérifier la connexion d'un utilisateur
     *
     * Retourne faux si aucune session n'est trouvée
     *
     * @access public
     * @return Manager
     */
    public function checkConnexion()
    {
        if (!empty($_SESSION['session_manager']) && !empty($_SESSION['session_ipaddr']) && !empty($_SESSION['session_last_access'])) {
            if (time() - $_SESSION['session_last_access'] < 14400) {
                if ($_SERVER['REMOTE_ADDR'] == $_SESSION['session_ipaddr']) {
                    $_SESSION['session_last_access'] = time();
                    if ($administrateur = $this->get($_SESSION['session_manager'])) {
                        return $administrateur;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public function total()
    {
        $requete = $this->bdd->prepare("SELECT COUNT(*) AS total FROM `manager`;");
        $requete->execute();
        $resultat = $requete->fetch(PDO::FETCH_ASSOC);

        return $resultat['total'];
    }
}
