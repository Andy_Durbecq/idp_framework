<?php

namespace models;

use \PDO;

class TicketManager
{

    /**
     * Instance de la connexion à la BDD
     *
     * @var object $bdd
     * @access private
     */
    private $bdd;

    /**
     * Constructeur de la classe
     *
     * @param object $bdd Lien de la base de données
     * @access public
     * @return void
     */
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    /**
     * Insertion
     *
     * @param Ticket $ticket
     * @access public
     * @return void
     */
    public function insert(Ticket $ticket)
    {
        $requete = $this->bdd->prepare("INSERT INTO `ticket` (`nom`, `prenom`, `telephone`, `email`, `demande`, `date`) VALUES (:nom, :prenom, :telephone, :email, :demande, :date);");
        $requete->execute(array(
            ":nom" => $ticket->nom,
            ":prenom" => $ticket->prenom,
            ":telephone" => $ticket->telephone,
            ":email" => $ticket->email,
            ":demande" => $ticket->demande,
            ":date" => $ticket->date
        ));
    }

    /**
     * Compte le nombre d'enregistrement
     *
     * @access public
     * @return int
     */
    public function count()
    {
        $requete = $this->bdd->query("SELECT * FROM `ticket`;");
        return count($requete->fetchAll());
    }

    /**
     * Suppression
     *
     * @param int $id Identifiant unique
     * @access public
     * @return void
     */
    public function delete($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `ticket` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }

    /**
     * Modification
     *
     * @param Ticket $ticket
     * @access public
     * @return void
     */
    public function update(Ticket $ticket)
    {
        $requete = $this->bdd->prepare("UPDATE `ticket` SET `nom` = :nom, `prenom` = :prenom, `telephone` = :telephone, `email` = :email, `demande` = :demande, `date` = :date WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $ticket->id,
            ":nom" => $ticket->nom,
            ":prenom" => $ticket->prenom,
            ":telephone" => $ticket->telephone,
            ":email" => $ticket->email,
            ":demande" => $ticket->demande,
            ":date" => $ticket->date
        ));
    }

    /**
     * Retourne une entrée
     *
     * @param int $id Identifiant unique
     * @access public
     * @return Ticket
     */
    public function get($id)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `ticket` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Ticket($donnees[0]);
        } else {
            return false;
        }
    }

    /**
     * Retourne toutes les entrées
     *
     * @access public
     * @return Array
     */
    public function getAll()
    {
        $retour = array();
        $requete = $this->bdd->query("SELECT * FROM `ticket`;");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Ticket($donnees);
        }
        return $retour;
    }
}
