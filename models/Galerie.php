<?php

namespace models;

class Galerie extends \core\Model
{

    /**
     * @var int $id
     * @access public
     */
    public $id;

    /**
     * @var varchar $nom
     * @access public
     */
    public $nom;

    /**
     * @var int $ordre
     * @access public
     */
    public $ordre;

    /*
     * Formatage avant affichage
     *
     * @return string
     * @access public
     */

    public function __tostring()
    {
        return $this->nom;
    }
}
