<?php

namespace models;

use \PDO;

class InternauteManager
{

    /**
     * Instance de la connexion à la BDD
     *
     * @var object $bdd
     * @access private
     */
    private $bdd;

    /**
     * Constructeur de la classe
     *
     * @param object $bdd Lien de la base de données
     * @access public
     * @return void
     */
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    /**
     * Insertion
     *
     * @param Internaute $internaute
     * @access public
     * @return void
     */
    public function insert(Internaute $internaute)
    {
        $requete = $this->bdd->prepare("INSERT INTO `internaute` (`id_facebook`, `token_facebook`, `nom`, `prenom`, `email`, `pass`, `actif`) VALUES (:id_facebook, :token_facebook, :nom, :prenom, :email, :pass, :actif);");
        $requete->execute(array(
            ":id_facebook" => $internaute->id_facebook,
            ":token_facebook" => $internaute->token_facebook,
            ":nom" => $internaute->nom,
            ":prenom" => $internaute->prenom,
            ":email" => $internaute->email,
            ":pass" => $internaute->pass,
            ":actif" => $internaute->actif
        ));
    }

    /**
     * Suppression
     *
     * @param int $id Identifiant unique
     * @access public
     * @return void
     */
    public function delete($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM `internaute` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
    }

    /**
     * Modification
     *
     * @param Internaute $internaute
     * @access public
     * @return void
     */
    public function update(Internaute $internaute)
    {
        $requete = $this->bdd->prepare("UPDATE `internaute` SET `id_facebook` = :id_facebook, `token_facebook` = :token_facebook, `nom` = :nom, `prenom` = :prenom, `email` = :email, `pass` = :pass, `actif` = :actif WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $internaute->id,
            ":id_facebook" => $internaute->id_facebook,
            ":token_facebook" => $internaute->token_facebook,
            ":nom" => $internaute->nom,
            ":prenom" => $internaute->prenom,
            ":email" => $internaute->email,
            ":pass" => $internaute->pass,
            ":actif" => $internaute->actif
        ));
    }

    /**
     * Retourne une entrée
     *
     * @param int $id Identifiant unique
     * @access public
     * @return Internaute
     */
    public function get($id)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `internaute` WHERE `id` = :id;");
        $requete->execute(array(
            ":id" => $id
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Internaute($donnees[0]);
        } else {
            return false;
        }
    }

    public function getEmail($email)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `internaute` WHERE `email` = :email;");
        $requete->execute(array(
            ":email" => $email
        ));
        $donnees = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (count($donnees) == 1) {
            return new Internaute($donnees[0]);
        } else {
            return false;
        }
    }

    /**
     * Retourne toutes les entrées
     *
     * @access public
     * @return Array
     */
    public function getAll()
    {
        $retour = array();
        $requete = $this->bdd->query("SELECT * FROM `internaute`;");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Internaute($donnees);
        }
        return $retour;
    }

    public function getPage($page = 1, $limite = 10, $where = "", $ordre = "id", $sens_ordre = "ASC")
    {
        $index_debut = ($page - 1) * $limite;
        $retour = array();
        $requete = $this->bdd->query("SELECT * FROM `internaute` WHERE `id` > 0" . $where . " ORDER BY `" . $ordre . "` " . $sens_ordre . " LIMIT " . $index_debut . ", " . $limite . ";");
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultat as $donnees) {
            $retour[] = new Internaute($donnees);
        }
        return $retour;
    }

    /**
     * Connexion
     *
     * @param string $email
     * @param string $pass
     * @access public
     * @return Internaute
     */
    public function connexion($email, $pass)
    {
        $requete = $this->bdd->prepare("SELECT * FROM `internaute` WHERE email = :email;");
        $requete->execute(array(":email" => $email));
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($resultat) && count($resultat) == 1) {
            if ($resultat[0]['pass'] == $pass) {
                $internaute = new Internaute($resultat[0]);
                if ($internaute->id && $internaute->actif) {
                    $_SESSION['session_internaute_id'] = $resultat[0]['id'];
                    $_SESSION['session_internaute_ipaddr'] = $_SERVER['REMOTE_ADDR'];
                    return $internaute;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Deconnexion
     *
     * @access public
     * @return void
     */
    public function deconnexion()
    {
        unset($_SESSION['session_internaute_id']);
        unset($_SESSION['session_internaute_ipaddr']);
        unset($_SESSION['facebook_access_token']);
    }

    /**
     * Permet de vérifier la connexion d'un internaute
     *
     * Retourne faux si aucune session n'est trouvée
     *
     * @access public
     * @return Internaute
     */
    public function checkConnexion()
    {
        if (!empty($_SESSION['session_internaute_id']) && !empty($_SESSION['session_internaute_ipaddr'])) {
            if ($_SERVER['REMOTE_ADDR'] == $_SESSION['session_internaute_ipaddr']) {
                if ($internaute = $this->get($_SESSION['session_internaute_id'])) {
                    if (!empty($_SESSION['facebook_access_token'])) {
                        if ($_SESSION['facebook_access_token'] == $internaute->token_facebook) {
                            return $internaute;
                        } else {
                            return false;
                        }
                    } else {
                        return $internaute;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function total()
    {
        $requete = $this->bdd->prepare("SELECT COUNT(*) AS total FROM `internaute`;");
        $requete->execute();
        $resultat = $requete->fetch(PDO::FETCH_ASSOC);

        return $resultat['total'];
    }
}
