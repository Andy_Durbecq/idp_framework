<?php

error_reporting(E_ALL);
ini_set("display_errors", false);

session_start();

require_once (__DIR__ . "/../vendor/autoload.php");
require_once (__DIR__ . "/../core/errors.php");

$application = new \app\App();
$application->run();
