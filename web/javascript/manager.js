$(document).ready(function () {

    /* DatePicker */
    if ($('.datepicker').length) {
        $(".datepicker").datepicker();
    }

    /* Sorting for photos - Posts */
    if ($(".sortable").length) {
        $(".sortable").sortable({
            placeholder: "ui-state-highlight",
            stop: function (event, ui) {
                $('#loader-ordre').fadeIn();
                ordre = 1;
                $('.sortable li').each(function () {
                    id_post_image = $(this).data('id');
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: '/manager/posts/ordre-image',
                        data: {
                            id_post_image: id_post_image,
                            ordre: ordre
                        }
                    });
                    $(this).find('.liste-doc-ordre').html(ordre);
                    ordre++;
                });
                $('#loader-ordre').fadeOut();
            }
        });
        $(".sortable").disableSelection();
    }

    /* Sorting gallery + change name */
    if ($(".sortable-galerie").length) {
        $(".sortable-galerie").sortable({
            placeholder: "ui-state-highlight",
            stop: function (event, ui) {
                $('#loader-ordre div').html('Tri en cours ...');
                $('#loader-ordre').fadeIn();
                ordre = 1;
                $('.sortable-galerie li').each(function () {
                    id_galerie = $(this).data('id');
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: '/manager/galerie-photo/modifier',
                        data: {
                            id_galerie: id_galerie,
                            ordre: ordre
                        }
                    });
                    ordre++;
                });
                $('#loader-ordre').fadeOut();
            }
        });

        /* Modify the name of the gallery */
        $('.sortable-galerie-modifier a').click(function () {
            id_galerie = $(this).parent().parent().data('id');
            nom_de_la_galerie = $(this).parent().parent().find('input').val();

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/manager/galerie-photo/modifier',
                data: {
                    id_galerie: id_galerie,
                    nom: nom_de_la_galerie
                },
                success: function (data) {
                    message("Nom de la galerie mis à jour", "info");
                }
            });

            return false;
        });

        /* Detail of the gallery */
        $('.sortable-galerie-select a').click(function () {
            $('.sortable-galerie li').removeClass('selected');
            $(this).parent().parent().addClass('selected');
            id_galerie = $(this).parent().parent().data('id');
            $('#upload-container').show();
            $('#loader-ordre div').html('Recherche ...');
            $('#loader-ordre').fadeIn();
            $('#upload-console').html('');
            getPhotos(id_galerie);
            addPlupload(id_galerie);
            return false;
        });

        /* Order photos from the selected gallery */
        $("#galerie-filelist").sortable({
            placeholder: "ui-state-highlight",
            stop: function (event, ui) {
                $('#loader-ordre div').html('Tri en cours ...');
                $('#loader-ordre').fadeIn();
                ordre = 1;
                $('#galerie-filelist .galerie-filelist-photo-sortable').each(function () {
                    id_galerie_photo = $(this).data('id');
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: '/manager/galerie-photo/modifier-photo',
                        data: {
                            id_galerie_photo: id_galerie_photo,
                            ordre: ordre
                        }
                    });
                    ordre++;
                });
                $('#loader-ordre').fadeOut();
            }
        });

        /* Change the alt of the photo */
        $('#galerie-filelist').on('click', '.galerie-filelist-photo-modifier a', function () {
            id_galerie_photo = $(this).parent().parent().data('id');
            nom_de_la_photo = $(this).parent().parent().find('input').val();

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/manager/galerie-photo/modifier-photo',
                data: {
                    id_galerie_photo: id_galerie_photo,
                    nom: nom_de_la_photo
                },
                success: function (data) {
                    message("Légende de la photo mise à jour", "info");
                }
            });

            return false;
        });

        /* Delete photo */
        $('#galerie-filelist').on('click', '.galerie-filelist-photo-supprimer a', function () {
            id_galerie_photo = $(this).parent().parent().data('id');
            ligne = $(this).parent().parent();

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/manager/galerie-photo/supprimer-photo',
                data: {
                    id_galerie_photo: id_galerie_photo
                },
                success: function (data) {
                    message("Photo supprimée", "info");
                    ligne.remove();
                }
            });

            return false;
        });
    }


});

/* Show error / report messages */
var timeMessage;
function message(texte, id) {
    if (typeof (timeMessage) != "undefined") {
        clearTimeout(timeMessage);
    }
    $('#message-info').hide();
    $('#message-info span').removeClass();
    $('#message-info span').html(texte);
    $('#message-info span').addClass(id);
    $('#message-info').fadeIn();
    timeMessage = setTimeout(function () {
        $('#message-info').fadeOut();
    }, 4000);
}

var uploader;
function addPlupload(id_galerie) {
    if (typeof (uploader) != "undefined") {
        uploader.destroy();
    }

    uploader = new plupload.Uploader({
        browse_button: 'upload-browse',
        url: '/manager/galerie-photo/ajouter-photo',
        flash_swf_url: '/javascript/Moxie.swf',
        filters: {
            max_file_size: 2097152,
            prevent_duplicates: true
        },
        drop_element: "upload-container",
        multipart_params: {
            "id_galerie": id_galerie
        }
    });

    uploader.init();

    uploader.bind('FilesAdded', function (up, files) {
        var html = '';
        plupload.each(files, function (file) {
            html += '<div id="' + file.id + '" class="galerie-filelist-photo"><div class="galerie-filelist-simulation-vignette"><span></span></div><div class="galerie-filelist-chargement">' + file.name + ' (' + plupload.formatSize(file.size) + ')</div></div>';
        });
        document.getElementById('galerie-filelist').innerHTML = html + document.getElementById('galerie-filelist').innerHTML;

        uploader.start();
    });

    uploader.bind('UploadProgress', function (up, file) {
        $('#' + file.id + ' .galerie-filelist-simulation-vignette span').html(file.percent + '%');
    });

    uploader.bind('Error', function (up, err) {
        $('#upload-console').fadeIn();
        document.getElementById('upload-console').innerHTML += "<p><strong>" + err.file.name + " </strong>: " + err.message + "</p>";
    });

    uploader.bind('FileUploaded', function (up, file, response) {
        data = $.parseJSON(response.response);
        $('#' + file.id).remove();
        if (data.error) {
            $('#upload-console').fadeIn();
            document.getElementById('upload-console').innerHTML += "<p><strong>" + file.name + " </strong>: " + data.message + "</p>";
        } else {
            html = '<div id="photo-' + data.id + '" data-id="' + data.id + '" class="galerie-filelist-photo galerie-filelist-photo-sortable">';
            html += '<div class="galerie-filelist-photo-img"><img src="/downloads/gallery/' + data.vignette + '" alt="" /></div>';
            html += '<div class="galerie-filelist-photo-input"><input type="text" name="nom_photo_' + data.id + '" value="" /></div>';
            html += '<div class="galerie-filelist-photo-modifier"><a href="#" title="Modifier la légende la photo"><i class="fa fa-fw fa-pencil-square-o"></i></a></div>';
            html += '<div class="galerie-filelist-photo-ordre"><a href="#" title="Modifier l\'ordre de la photo par glisser-déposer" onclick="return false;"><i class="fa fa-fw fa-sort"></i></a></div>';
            html += '<div class="galerie-filelist-photo-supprimer"><a href="#" title="Supprimer la photo"><i class="fa fa-fw fa-trash-o"></i></a></div>';
            html += '</div>';
            document.getElementById('galerie-filelist').innerHTML = html + document.getElementById('galerie-filelist').innerHTML;
        }
    });
}

/* Returns the photos of a selected gallery */
function getPhotos(id_galerie) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/manager/galerie-photo/get-photos',
        data: {
            id_galerie: id_galerie
        },
        success: function (data) {
            $('#galerie-filelist').html(data.html);
            $('#loader-ordre').fadeOut();
        }
    });
}