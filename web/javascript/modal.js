/**
 * modal.initialise(); => Before any call
 * modal.open({option});
 */
var modal = {
    overlay: document.createElement('div'),
    modal: document.createElement('div'),
    close: null,
    content: null,
    is_open: false,
    settings: {
        width: '',
        height: '',
        content: '',
        class_name: ''
    },
    open: function (settings_args) {
        this.settings.width = settings_args.width || "auto";
        this.settings.height = settings_args.height || "auto";
        this.settings.content = settings_args.content || "Undefined";
        this.settings.class_name = settings_args.class_name || "modal-generique";

        this.content.innerHTML = this.settings.content;

        this.modal.className = this.settings.class_name;

        var largeur_pop = this.settings.width;
        if (this.settings.width > window.innerWidth) {
            largeur_pop = window.innerWidth * 80 / 100;
        }
        var hauteur_pop = this.settings.height;
        if (this.settings.height > window.innerHeight) {
            hauteur_pop = window.innerHeight * 80 / 100;
        }
        this.modal.style.cssText = "width: " + largeur_pop + "px; height: " + hauteur_pop + "px;";

        this.modal.style.display = "block";
        this.overlay.style.display = "block";

        this.is_open = true;
    },    
    close: function () {
        this.modal.className = "";
        this.modal.style.display = "none";
        this.overlay.style.display = "none";
        this.content.innerHTML = "";
        this.is_open = false;
    },
    redimModal: function () {
        if (this.is_open) {
            this.overlay.style.width = window.innerWidth + "px";
            this.overlay.style.height = window.innerHeight + "px";

            var largeur_pop = this.settings.width;
            if (this.settings.width > window.innerWidth) {
                largeur_pop = window.innerWidth * 80 / 100;
            }
            var hauteur_pop = this.settings.height;
            if (this.settings.height > window.innerHeight) {
                hauteur_pop = window.innerHeight * 80 / 100;
            }
            this.modal.style.cssText = "width: " + largeur_pop + "px; height: " + hauteur_pop + "px;";
        }
    },
    initialise: function () {
        _self = this;
        this.overlay.id = "overlay";
        this.overlay.style.cssText = "display: none; width: " + window.innerWidth + "px; height: " + window.innerHeight + "px;";

        this.modal.id = "modal";
        this.modal.style.display = "none";
        this.modal.innerHTML = '<a id="modal-close" href="#"><i class="fa fa-fw fa-close"></i></a><div id="modal-content"><div id="modal-content-pad"></div></div>';

        document.body.appendChild(this.overlay);
        document.body.appendChild(this.modal);

        this.close = document.querySelector("#modal #modal-close");
        this.content = document.querySelector("#modal #modal-content-pad");

        this.close.onclick = function (e) {
            e.preventDefault();
            _self.close(); 
        };

        this.overlay.onclick = function (e) {
            e.preventDefault();
            _self.close();
        };

        window.addEventListener("resize", function () {
            _self.redimModal();
        }, false);
    }
};