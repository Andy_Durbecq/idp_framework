function pageLoad() {
   
}

/* Page loaded */
if (window.addEventListener) {
    window.addEventListener("load", pageLoad, false);
} else if (window.attachEvent) {
    window.attachEvent("onload", pageLoad);
} else {
    window.onload = pageLoad;
}

/* Show error / report messages */
var timeMessage;
function message(texte, id) {
    if (typeof (timeMessage) != "undefined") {
        clearTimeout(timeMessage);
    }

    var message_info = document.querySelector("#message-info");
    var message_info_span = document.querySelector("#message-info span");
    message_info_span.className = id;
    message_info_span.innerHTML = texte;
    message_info.className = "show";
    timeMessage = setTimeout(function () {
        message_info.className = "";
    }, 4000);
}